﻿using System.Collections.Generic;

namespace ASEAPROJECT.Authentication.External
{
    public interface IExternalAuthConfiguration
    {
        List<ExternalLoginProviderInfo> Providers { get; }
    }
}
