using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace ASEAPROJECT.Controllers
{
    public abstract class ASEAPROJECTControllerBase: AbpController
    {
        protected ASEAPROJECTControllerBase()
        {
            LocalizationSourceName = ASEAPROJECTConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
