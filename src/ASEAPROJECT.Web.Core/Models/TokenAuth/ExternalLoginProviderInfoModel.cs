﻿using Abp.AutoMapper;
using ASEAPROJECT.Authentication.External;

namespace ASEAPROJECT.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
