﻿namespace ASEAPROJECT
{
    public class ASEAPROJECTConsts
    {
        public const string LocalizationSourceName = "ASEAPROJECT";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
