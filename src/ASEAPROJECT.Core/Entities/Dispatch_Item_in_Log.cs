﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ASEAPROJECT.Entities
{
    [Table("dispatch_items_in_logs")]
    public class Dispatch_Item_in_Log : Entity
    {
        public int log_id { get; set; }

        public int item_id { get; set; }

        [ForeignKey("log_id")]
        public DailyVerificationLog log { get; set; }

        [ForeignKey("ietm_id")]
        public DispatchItem item { get; set; }
    }
}
