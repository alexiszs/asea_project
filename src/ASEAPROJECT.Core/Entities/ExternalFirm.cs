﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ASEAPROJECT.Entities
{
    [Table("external_firms")]
    public class ExternalFirm : Entity
    {
        [MaxLength(300)]
        [Required]
        public string tradename { get; set; }
        [MaxLength(300)]
        [Required]
        public string company_name { get; set; }
        [MaxLength(300)]
        [Required]
        public string addres { get; set; }
        [MaxLength(300)]
        [Required]
        public string email { get; set; }

        public ICollection<Log> logs { get; set; }
        public ICollection<RolesFirms> roles {get; set;}
    }
}
