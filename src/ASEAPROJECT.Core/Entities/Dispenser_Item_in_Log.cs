﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ASEAPROJECT.Entities
{
    [Table("dispeser_items_in_logss")]
    public class Dispenser_Item_in_Log : Entity
    {
        public int log_id { get; set; }

        public int item_id { get; set; }

        [ForeignKey("log_id")]
        public DailyVerificationLog log { get; set; }

        [ForeignKey("item_id")]
        public DispenserItem item { get; set; }
    }
}
