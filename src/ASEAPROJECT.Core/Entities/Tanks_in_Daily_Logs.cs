﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ASEAPROJECT.Entities
{
    [Table("tanks_in_daily_logs")]
    public class Tanks_in_Daily_Logs :Entity
    {

        public int log_id { get; set; }

        public int tank_id { get; set; }

        [ForeignKey("log_id")]
        public DailyVerificationLog log { get; set; }

        [ForeignKey("tank_id")]
        public Tank tank { get; set; }
    }
}
