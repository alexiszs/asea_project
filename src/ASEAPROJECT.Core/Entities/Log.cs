﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASEAPROJECT.Entities
{
    [Table("logs")]
    public class Log : AuditedEntity, ISoftDelete
    {

        [Required]
        public DateTime date { get; set; }
        [Required]
        public DateTime begin { get; set; }
        [Required]
        public DateTime end { get; set; }

        [MaxLength(100)]
        [Required]
        public string certication_number { get; set; }

        [MaxLength(5000)]
        public string comments { get; set; }

        public int frecuency_id { get; set; }

        public int event_type_id { get; set; }

        public int firm_id { get; set; }

        [ForeignKey("frecuency_id")]
        public FrecuencyOfActivity frecuency { get; set; }

        [ForeignKey("event_type_id")]
        public Event event_type { get; set; }

        [ForeignKey("firm_id")]
        public ExternalFirm firm { get; set; }

        public virtual bool IsDeleted { get; set; }
    }
}