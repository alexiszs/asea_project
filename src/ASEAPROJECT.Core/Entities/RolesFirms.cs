﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASEAPROJECT.Entities
{
    [Table("roles_firms")]
    public class RolesFirms: Entity
    {
        public int firm_id { get; set; }

        public int role_id { get; set; }

        [ForeignKey("firm_id")]
        public ExternalFirm firm { get; set; }

        [ForeignKey("role_id")]
        public ExternalRol role { get; set; }
    }
}
