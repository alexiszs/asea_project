﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ASEAPROJECT.Entities
{
    [Table("residues_in_logs")]
    public class Residues_in_Log : Entity
    {

        public int log_id { get; set; }

        public int residue_id { get; set; }

        [ForeignKey("log_id")]
        public DailyVerificationLog log { get; set; }

        [ForeignKey("residue_log")]
        public DangerResidues residue { get; set; }
    }
}
