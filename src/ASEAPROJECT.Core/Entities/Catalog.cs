﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ASEAPROJECT.Entities
{
    [Table("catalogs")]
    public class Catalog : Entity
    {
        [MaxLength(50)]
        [Required]
        public string name { get; set; }

        public string prefix { get; set; }
    }
}
