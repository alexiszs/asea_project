﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ASEAPROJECT.Entities
{
    [Table("dailyveryficationlogs")]
    public class DailyVerificationLog: AuditedEntity, ISoftDelete
    {
        public DateTime date { get; set; }

        public DateTime hour { get; set; }

        public string folio { get; set; }

        public ICollection<Tanks_in_Daily_Logs> tanks { get; set; }

        public ICollection<Machinery_in_Daily_logs> machinary { get; set; }

        public ICollection<Residues_in_Log> residues { get; set; }

        public ICollection<Mens_Bathroom_in_Log> Mbathroom { get; set; }

        public ICollection<Womens_Bathroom_in_Log> Wbathroom { get; set; }

        public ICollection<Electric_Room_in_Log> ElectrictRoom { get; set; }

        public ICollection<Grocery_Item_in_log> grocery { get; set; }

        public ICollection<Common_Item_in_log> common { get; set; }

        public ICollection<Dispenser_Item_in_Log> dispenser { get; set; }

        public ICollection<Dispatch_Item_in_Log> dispatch { get; set; }

        public ICollection<Employees_Item_in_Log> employees { get; set; }

        public ICollection<Extinguisher_Item_in_Log> extinguisher { get; set; }

        public string comments { get; set; }
        public bool IsDeleted  { get; set; }
}
}
