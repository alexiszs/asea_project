﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ASEAPROJECT.Entities
{
    [Table("electric_room_in_logs")]
    public class Electric_Room_in_Log : Entity
    {

        public int log_id { get; set; }

        public int equip_id { get; set; }

        [ForeignKey("log_id")]
        public DailyVerificationLog log { get; set; }

        [ForeignKey("equip_id")]
        public ElectricEquip equip { get; set; }
    }
}
