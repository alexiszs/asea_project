﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ASEAPROJECT.Entities
{
    [Table("mens_bathroom_in_logs")]
    public class Mens_Bathroom_in_Log : Entity
    {
        public int log_id { get; set; }

        public int bathroom_id { get; set; }

        [ForeignKey("log_id")]
        public DailyVerificationLog log { get; set; }

        [ForeignKey("bathroom_id")]
        public MensBathroom bathroom { get; set; }
    }
}
