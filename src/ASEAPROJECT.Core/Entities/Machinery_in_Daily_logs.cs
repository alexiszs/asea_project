﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASEAPROJECT.Entities
{
    [Table("machinery_in_daily_logs")]
    public class Machinery_in_Daily_logs : Entity
    {

        public int log_id { get; set; }

        public int machine_id { get; set; }

        [ForeignKey("log_id")]
        public DailyVerificationLog log { get; set; }


        [ForeignKey("machine_id")]
        public Machine machine { get; set; }
    }
}
