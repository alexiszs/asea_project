﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ASEAPROJECT.Entities
{
    [Table("residuallogs")]
   public class ResidualLog : AuditedEntity, ISoftDelete
    {
        [Required]
        public string residueType { get; set; }

        [Required]
        public Byte personalAuth { get; set; }

        [Required]
        public string civilpermission { get; set; }

        [Required]
        public string certnumber { get; set; }

        [Required]
        public string manifestnumber { get; set; }

        [Required]
        public DateTime emissiondate { get; set; }

        [Required]
        public int solidrests { get; set; }

        [Required]
        public int liquidrests { get; set; }

        public string comments { get; set; }

        public int? extraction_firm_id { get; set; }

        public int? shipping_firm_id { get; set; }

        public int? reception_firm_id { get; set; }

        public int temp_storage_id { get; set; }

        [ForeignKey("extraction_firm_id")]
        public ExternalFirm extraction_firm { get; set; }

        [ForeignKey("shipping_firm_id")]
        public ExternalFirm shipping_firm { get; set; }

        [ForeignKey("reception_firm_id")]
        public ExternalFirm reception_firm { get; set; }

        [ForeignKey("temp_storage_id")]
        public TemporyStorage tempory_storage { get; set; }

        public ICollection<EquipItem> equipment { get; set; }

        public virtual bool IsDeleted { get; set; }
    }
}
