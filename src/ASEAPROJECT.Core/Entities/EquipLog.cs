﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ASEAPROJECT.Entities
{
    [Table("equip_logs")]
    public class EquipLog: Entity
    {
        public int residual_log_id { get; set; }

        public int equip_id { get; set; }

        [ForeignKey("residual_log_id")]
        public ResidualLog ResidualLog { get; set; }

        [ForeignKey("equip_id")]
        public EquipItem equip { get; set; }
    }
}
