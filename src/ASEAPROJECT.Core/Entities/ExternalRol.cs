﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ASEAPROJECT.Entities
{
    [Table("externalroles")]
    public class ExternalRol: Entity
    {
        public string name { get; set; }
    }
}
