﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ASEAPROJECT.Entities
{
    [Table("commons_items_in_logs")]
    public class Common_Item_in_log : Entity
    {
        public int log_id { get; set; }

        public int item_id { get; set; }

        [ForeignKey("log_id")]
        public DailyVerificationLog log { get; set; }

        [ForeignKey("item_id")]
        public CommonItems item { get; set; }
    }
}
