﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ASEAPROJECT.Entities
{
    [Table("womens_bathrooms_in_logs")]
    public class Womens_Bathroom_in_Log : Entity
    {

        public int log_id { get; set; }

        public int bathroom_id { get; set; }

        [ForeignKey("log_id")]
        public DailyVerificationLog log { get; set; }

        [ForeignKey("bathroom_id")]
        public WomensBathroom WBathroom { get; set; }
    }
}
