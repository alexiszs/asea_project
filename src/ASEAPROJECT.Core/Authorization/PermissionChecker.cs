﻿using Abp.Authorization;
using ASEAPROJECT.Authorization.Roles;
using ASEAPROJECT.Authorization.Users;

namespace ASEAPROJECT.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
