﻿using Abp.MultiTenancy;
using ASEAPROJECT.Authorization.Users;

namespace ASEAPROJECT.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
