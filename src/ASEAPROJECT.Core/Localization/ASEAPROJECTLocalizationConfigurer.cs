﻿using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace ASEAPROJECT.Localization
{
    public static class ASEAPROJECTLocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(ASEAPROJECTConsts.LocalizationSourceName,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(ASEAPROJECTLocalizationConfigurer).GetAssembly(),
                        "ASEAPROJECT.Localization.SourceFiles"
                    )
                )
            );
        }
    }
}
