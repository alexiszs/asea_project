using Microsoft.AspNetCore.Antiforgery;
using ASEAPROJECT.Controllers;

namespace ASEAPROJECT.Web.Host.Controllers
{
    public class AntiForgeryController : ASEAPROJECTControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
