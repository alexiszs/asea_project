﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ASEAPROJECT.Configuration;

namespace ASEAPROJECT.Web.Host.Startup
{
    [DependsOn(
       typeof(ASEAPROJECTWebCoreModule))]
    public class ASEAPROJECTWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public ASEAPROJECTWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ASEAPROJECTWebHostModule).GetAssembly());
        }
    }
}
