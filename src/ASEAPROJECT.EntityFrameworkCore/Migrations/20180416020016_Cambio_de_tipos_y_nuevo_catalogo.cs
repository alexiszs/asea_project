﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ASEAPROJECT.Migrations
{
    public partial class Cambio_de_tipos_y_nuevo_catalogo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "tempsavage",
                table: "residuallogs");

            migrationBuilder.AddColumn<int>(
                name: "temp_storage_id",
                table: "residuallogs",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_residuallogs_temp_storage_id",
                table: "residuallogs",
                column: "temp_storage_id");

            migrationBuilder.AddForeignKey(
                name: "FK_residuallogs_catalogs_temp_storage_id",
                table: "residuallogs",
                column: "temp_storage_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_residuallogs_catalogs_temp_storage_id",
                table: "residuallogs");

            migrationBuilder.DropIndex(
                name: "IX_residuallogs_temp_storage_id",
                table: "residuallogs");

            migrationBuilder.DropColumn(
                name: "temp_storage_id",
                table: "residuallogs");

            migrationBuilder.AddColumn<string>(
                name: "tempsavage",
                table: "residuallogs",
                nullable: false,
                defaultValue: "");
        }
    }
}
