﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ASEAPROJECT.Migrations
{
    public partial class DailyVerificationLog : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "dailyveryficationlogs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    comments = table.Column<string>(nullable: true),
                    date = table.Column<DateTime>(nullable: false),
                    folio = table.Column<string>(nullable: true),
                    hour = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dailyveryficationlogs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "commons_items_in_logs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    item_id = table.Column<int>(nullable: false),
                    log_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_commons_items_in_logs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_commons_items_in_logs_catalogs_item_id",
                        column: x => x.item_id,
                        principalTable: "catalogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_commons_items_in_logs_dailyveryficationlogs_log_id",
                        column: x => x.log_id,
                        principalTable: "dailyveryficationlogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "dispatch_items_in_logs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ietm_id = table.Column<int>(nullable: true),
                    item_id = table.Column<int>(nullable: false),
                    log_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dispatch_items_in_logs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_dispatch_items_in_logs_catalogs_ietm_id",
                        column: x => x.ietm_id,
                        principalTable: "catalogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dispatch_items_in_logs_dailyveryficationlogs_log_id",
                        column: x => x.log_id,
                        principalTable: "dailyveryficationlogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "dispeser_items_in_logss",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    item_id = table.Column<int>(nullable: false),
                    log_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dispeser_items_in_logss", x => x.Id);
                    table.ForeignKey(
                        name: "FK_dispeser_items_in_logss_catalogs_item_id",
                        column: x => x.item_id,
                        principalTable: "catalogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dispeser_items_in_logss_dailyveryficationlogs_log_id",
                        column: x => x.log_id,
                        principalTable: "dailyveryficationlogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "electric_room_in_logs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    equip_id = table.Column<int>(nullable: false),
                    log_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_electric_room_in_logs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_electric_room_in_logs_catalogs_equip_id",
                        column: x => x.equip_id,
                        principalTable: "catalogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_electric_room_in_logs_dailyveryficationlogs_log_id",
                        column: x => x.log_id,
                        principalTable: "dailyveryficationlogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "employees_items_in_logs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    item_id = table.Column<int>(nullable: false),
                    log_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_employees_items_in_logs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_employees_items_in_logs_catalogs_item_id",
                        column: x => x.item_id,
                        principalTable: "catalogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_employees_items_in_logs_dailyveryficationlogs_log_id",
                        column: x => x.log_id,
                        principalTable: "dailyveryficationlogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "extinguisher_items_in_logs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    item_id = table.Column<int>(nullable: false),
                    log_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_extinguisher_items_in_logs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_extinguisher_items_in_logs_catalogs_item_id",
                        column: x => x.item_id,
                        principalTable: "catalogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_extinguisher_items_in_logs_dailyveryficationlogs_log_id",
                        column: x => x.log_id,
                        principalTable: "dailyveryficationlogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "grocery_items_in_logs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    item_id = table.Column<int>(nullable: false),
                    log_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_grocery_items_in_logs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_grocery_items_in_logs_catalogs_item_id",
                        column: x => x.item_id,
                        principalTable: "catalogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_grocery_items_in_logs_dailyveryficationlogs_log_id",
                        column: x => x.log_id,
                        principalTable: "dailyveryficationlogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "machinery_in_daily_logs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    log_id = table.Column<int>(nullable: false),
                    machine_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_machinery_in_daily_logs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_machinery_in_daily_logs_dailyveryficationlogs_log_id",
                        column: x => x.log_id,
                        principalTable: "dailyveryficationlogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_machinery_in_daily_logs_catalogs_machine_id",
                        column: x => x.machine_id,
                        principalTable: "catalogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "mens_bathroom_in_logs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    bathroom_id = table.Column<int>(nullable: false),
                    log_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mens_bathroom_in_logs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_mens_bathroom_in_logs_catalogs_bathroom_id",
                        column: x => x.bathroom_id,
                        principalTable: "catalogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_mens_bathroom_in_logs_dailyveryficationlogs_log_id",
                        column: x => x.log_id,
                        principalTable: "dailyveryficationlogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "residues_in_logs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    log_id = table.Column<int>(nullable: false),
                    residue_id = table.Column<int>(nullable: false),
                    residue_log = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_residues_in_logs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_residues_in_logs_dailyveryficationlogs_log_id",
                        column: x => x.log_id,
                        principalTable: "dailyveryficationlogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_residues_in_logs_catalogs_residue_log",
                        column: x => x.residue_log,
                        principalTable: "catalogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "tanks_in_daily_logs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    log_id = table.Column<int>(nullable: false),
                    tank_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tanks_in_daily_logs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tanks_in_daily_logs_dailyveryficationlogs_log_id",
                        column: x => x.log_id,
                        principalTable: "dailyveryficationlogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_tanks_in_daily_logs_catalogs_tank_id",
                        column: x => x.tank_id,
                        principalTable: "catalogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "womens_bathrooms_in_logs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    bathroom_id = table.Column<int>(nullable: false),
                    log_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_womens_bathrooms_in_logs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_womens_bathrooms_in_logs_catalogs_bathroom_id",
                        column: x => x.bathroom_id,
                        principalTable: "catalogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_womens_bathrooms_in_logs_dailyveryficationlogs_log_id",
                        column: x => x.log_id,
                        principalTable: "dailyveryficationlogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_commons_items_in_logs_item_id",
                table: "commons_items_in_logs",
                column: "item_id");

            migrationBuilder.CreateIndex(
                name: "IX_commons_items_in_logs_log_id",
                table: "commons_items_in_logs",
                column: "log_id");

            migrationBuilder.CreateIndex(
                name: "IX_dispatch_items_in_logs_ietm_id",
                table: "dispatch_items_in_logs",
                column: "ietm_id");

            migrationBuilder.CreateIndex(
                name: "IX_dispatch_items_in_logs_log_id",
                table: "dispatch_items_in_logs",
                column: "log_id");

            migrationBuilder.CreateIndex(
                name: "IX_dispeser_items_in_logss_item_id",
                table: "dispeser_items_in_logss",
                column: "item_id");

            migrationBuilder.CreateIndex(
                name: "IX_dispeser_items_in_logss_log_id",
                table: "dispeser_items_in_logss",
                column: "log_id");

            migrationBuilder.CreateIndex(
                name: "IX_electric_room_in_logs_equip_id",
                table: "electric_room_in_logs",
                column: "equip_id");

            migrationBuilder.CreateIndex(
                name: "IX_electric_room_in_logs_log_id",
                table: "electric_room_in_logs",
                column: "log_id");

            migrationBuilder.CreateIndex(
                name: "IX_employees_items_in_logs_item_id",
                table: "employees_items_in_logs",
                column: "item_id");

            migrationBuilder.CreateIndex(
                name: "IX_employees_items_in_logs_log_id",
                table: "employees_items_in_logs",
                column: "log_id");

            migrationBuilder.CreateIndex(
                name: "IX_extinguisher_items_in_logs_item_id",
                table: "extinguisher_items_in_logs",
                column: "item_id");

            migrationBuilder.CreateIndex(
                name: "IX_extinguisher_items_in_logs_log_id",
                table: "extinguisher_items_in_logs",
                column: "log_id");

            migrationBuilder.CreateIndex(
                name: "IX_grocery_items_in_logs_item_id",
                table: "grocery_items_in_logs",
                column: "item_id");

            migrationBuilder.CreateIndex(
                name: "IX_grocery_items_in_logs_log_id",
                table: "grocery_items_in_logs",
                column: "log_id");

            migrationBuilder.CreateIndex(
                name: "IX_machinery_in_daily_logs_log_id",
                table: "machinery_in_daily_logs",
                column: "log_id");

            migrationBuilder.CreateIndex(
                name: "IX_machinery_in_daily_logs_machine_id",
                table: "machinery_in_daily_logs",
                column: "machine_id");

            migrationBuilder.CreateIndex(
                name: "IX_mens_bathroom_in_logs_bathroom_id",
                table: "mens_bathroom_in_logs",
                column: "bathroom_id");

            migrationBuilder.CreateIndex(
                name: "IX_mens_bathroom_in_logs_log_id",
                table: "mens_bathroom_in_logs",
                column: "log_id");

            migrationBuilder.CreateIndex(
                name: "IX_residues_in_logs_log_id",
                table: "residues_in_logs",
                column: "log_id");

            migrationBuilder.CreateIndex(
                name: "IX_residues_in_logs_residue_log",
                table: "residues_in_logs",
                column: "residue_log");

            migrationBuilder.CreateIndex(
                name: "IX_tanks_in_daily_logs_log_id",
                table: "tanks_in_daily_logs",
                column: "log_id");

            migrationBuilder.CreateIndex(
                name: "IX_tanks_in_daily_logs_tank_id",
                table: "tanks_in_daily_logs",
                column: "tank_id");

            migrationBuilder.CreateIndex(
                name: "IX_womens_bathrooms_in_logs_bathroom_id",
                table: "womens_bathrooms_in_logs",
                column: "bathroom_id");

            migrationBuilder.CreateIndex(
                name: "IX_womens_bathrooms_in_logs_log_id",
                table: "womens_bathrooms_in_logs",
                column: "log_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "commons_items_in_logs");

            migrationBuilder.DropTable(
                name: "dispatch_items_in_logs");

            migrationBuilder.DropTable(
                name: "dispeser_items_in_logss");

            migrationBuilder.DropTable(
                name: "electric_room_in_logs");

            migrationBuilder.DropTable(
                name: "employees_items_in_logs");

            migrationBuilder.DropTable(
                name: "extinguisher_items_in_logs");

            migrationBuilder.DropTable(
                name: "grocery_items_in_logs");

            migrationBuilder.DropTable(
                name: "machinery_in_daily_logs");

            migrationBuilder.DropTable(
                name: "mens_bathroom_in_logs");

            migrationBuilder.DropTable(
                name: "residues_in_logs");

            migrationBuilder.DropTable(
                name: "tanks_in_daily_logs");

            migrationBuilder.DropTable(
                name: "womens_bathrooms_in_logs");

            migrationBuilder.DropTable(
                name: "dailyveryficationlogs");
        }
    }
}
