﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ASEAPROJECT.Migrations
{
    public partial class ExternalRoleAdd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_catalogs_residuallogs_ResidualLogId",
                table: "catalogs");

            migrationBuilder.DropForeignKey(
                name: "FK_commons_items_in_logs_catalogs_item_id",
                table: "commons_items_in_logs");

            migrationBuilder.DropForeignKey(
                name: "FK_dispatch_items_in_logs_catalogs_ietm_id",
                table: "dispatch_items_in_logs");

            migrationBuilder.DropForeignKey(
                name: "FK_dispeser_items_in_logss_catalogs_item_id",
                table: "dispeser_items_in_logss");

            migrationBuilder.DropForeignKey(
                name: "FK_electric_room_in_logs_catalogs_equip_id",
                table: "electric_room_in_logs");

            migrationBuilder.DropForeignKey(
                name: "FK_employees_items_in_logs_catalogs_item_id",
                table: "employees_items_in_logs");

            migrationBuilder.DropForeignKey(
                name: "FK_equip_logs_catalogs_equip_id",
                table: "equip_logs");

            migrationBuilder.DropForeignKey(
                name: "FK_extinguisher_items_in_logs_catalogs_item_id",
                table: "extinguisher_items_in_logs");

            migrationBuilder.DropForeignKey(
                name: "FK_grocery_items_in_logs_catalogs_item_id",
                table: "grocery_items_in_logs");

            migrationBuilder.DropForeignKey(
                name: "FK_logs_catalogs_event_type_id",
                table: "logs");

            migrationBuilder.DropForeignKey(
                name: "FK_logs_catalogs_frecuency_id",
                table: "logs");

            migrationBuilder.DropForeignKey(
                name: "FK_machinery_in_daily_logs_catalogs_machine_id",
                table: "machinery_in_daily_logs");

            migrationBuilder.DropForeignKey(
                name: "FK_mens_bathroom_in_logs_catalogs_bathroom_id",
                table: "mens_bathroom_in_logs");

            migrationBuilder.DropForeignKey(
                name: "FK_residuallogs_catalogs_temp_storage_id",
                table: "residuallogs");

            migrationBuilder.DropForeignKey(
                name: "FK_residues_in_logs_catalogs_residue_log",
                table: "residues_in_logs");

            migrationBuilder.DropForeignKey(
                name: "FK_roles_firms_catalogs_role_id",
                table: "roles_firms");

            migrationBuilder.DropForeignKey(
                name: "FK_tanks_in_daily_logs_catalogs_tank_id",
                table: "tanks_in_daily_logs");

            migrationBuilder.DropForeignKey(
                name: "FK_womens_bathrooms_in_logs_catalogs_bathroom_id",
                table: "womens_bathrooms_in_logs");

            migrationBuilder.DropPrimaryKey(
                name: "PK_catalogs",
                table: "catalogs");

            migrationBuilder.DropIndex(
                name: "IX_catalogs_ResidualLogId",
                table: "catalogs");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "catalogs");

            migrationBuilder.DropColumn(
                name: "prefix",
                table: "catalogs");

            migrationBuilder.DropColumn(
                name: "ResidualLogId",
                table: "catalogs");

            migrationBuilder.RenameTable(
                name: "catalogs",
                newName: "externalroles");

            migrationBuilder.AlterColumn<string>(
                name: "name",
                table: "externalroles",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AddPrimaryKey(
                name: "PK_externalroles",
                table: "externalroles",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "catalogs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Discriminator = table.Column<string>(nullable: false),
                    name = table.Column<string>(maxLength: 50, nullable: false),
                    prefix = table.Column<string>(nullable: true),
                    ResidualLogId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_catalogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_catalogs_residuallogs_ResidualLogId",
                        column: x => x.ResidualLogId,
                        principalTable: "residuallogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_catalogs_ResidualLogId",
                table: "catalogs",
                column: "ResidualLogId");

            migrationBuilder.AddForeignKey(
                name: "FK_commons_items_in_logs_catalogs_item_id",
                table: "commons_items_in_logs",
                column: "item_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dispatch_items_in_logs_catalogs_ietm_id",
                table: "dispatch_items_in_logs",
                column: "ietm_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dispeser_items_in_logss_catalogs_item_id",
                table: "dispeser_items_in_logss",
                column: "item_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_electric_room_in_logs_catalogs_equip_id",
                table: "electric_room_in_logs",
                column: "equip_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_employees_items_in_logs_catalogs_item_id",
                table: "employees_items_in_logs",
                column: "item_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_equip_logs_catalogs_equip_id",
                table: "equip_logs",
                column: "equip_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_extinguisher_items_in_logs_catalogs_item_id",
                table: "extinguisher_items_in_logs",
                column: "item_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_grocery_items_in_logs_catalogs_item_id",
                table: "grocery_items_in_logs",
                column: "item_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_logs_catalogs_event_type_id",
                table: "logs",
                column: "event_type_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_logs_catalogs_frecuency_id",
                table: "logs",
                column: "frecuency_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_machinery_in_daily_logs_catalogs_machine_id",
                table: "machinery_in_daily_logs",
                column: "machine_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_mens_bathroom_in_logs_catalogs_bathroom_id",
                table: "mens_bathroom_in_logs",
                column: "bathroom_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_residuallogs_catalogs_temp_storage_id",
                table: "residuallogs",
                column: "temp_storage_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_residues_in_logs_catalogs_residue_log",
                table: "residues_in_logs",
                column: "residue_log",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_roles_firms_externalroles_role_id",
                table: "roles_firms",
                column: "role_id",
                principalTable: "externalroles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_tanks_in_daily_logs_catalogs_tank_id",
                table: "tanks_in_daily_logs",
                column: "tank_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_womens_bathrooms_in_logs_catalogs_bathroom_id",
                table: "womens_bathrooms_in_logs",
                column: "bathroom_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_commons_items_in_logs_catalogs_item_id",
                table: "commons_items_in_logs");

            migrationBuilder.DropForeignKey(
                name: "FK_dispatch_items_in_logs_catalogs_ietm_id",
                table: "dispatch_items_in_logs");

            migrationBuilder.DropForeignKey(
                name: "FK_dispeser_items_in_logss_catalogs_item_id",
                table: "dispeser_items_in_logss");

            migrationBuilder.DropForeignKey(
                name: "FK_electric_room_in_logs_catalogs_equip_id",
                table: "electric_room_in_logs");

            migrationBuilder.DropForeignKey(
                name: "FK_employees_items_in_logs_catalogs_item_id",
                table: "employees_items_in_logs");

            migrationBuilder.DropForeignKey(
                name: "FK_equip_logs_catalogs_equip_id",
                table: "equip_logs");

            migrationBuilder.DropForeignKey(
                name: "FK_extinguisher_items_in_logs_catalogs_item_id",
                table: "extinguisher_items_in_logs");

            migrationBuilder.DropForeignKey(
                name: "FK_grocery_items_in_logs_catalogs_item_id",
                table: "grocery_items_in_logs");

            migrationBuilder.DropForeignKey(
                name: "FK_logs_catalogs_event_type_id",
                table: "logs");

            migrationBuilder.DropForeignKey(
                name: "FK_logs_catalogs_frecuency_id",
                table: "logs");

            migrationBuilder.DropForeignKey(
                name: "FK_machinery_in_daily_logs_catalogs_machine_id",
                table: "machinery_in_daily_logs");

            migrationBuilder.DropForeignKey(
                name: "FK_mens_bathroom_in_logs_catalogs_bathroom_id",
                table: "mens_bathroom_in_logs");

            migrationBuilder.DropForeignKey(
                name: "FK_residuallogs_catalogs_temp_storage_id",
                table: "residuallogs");

            migrationBuilder.DropForeignKey(
                name: "FK_residues_in_logs_catalogs_residue_log",
                table: "residues_in_logs");

            migrationBuilder.DropForeignKey(
                name: "FK_roles_firms_externalroles_role_id",
                table: "roles_firms");

            migrationBuilder.DropForeignKey(
                name: "FK_tanks_in_daily_logs_catalogs_tank_id",
                table: "tanks_in_daily_logs");

            migrationBuilder.DropForeignKey(
                name: "FK_womens_bathrooms_in_logs_catalogs_bathroom_id",
                table: "womens_bathrooms_in_logs");

            migrationBuilder.DropTable(
                name: "catalogs");

            migrationBuilder.DropPrimaryKey(
                name: "PK_externalroles",
                table: "externalroles");

            migrationBuilder.RenameTable(
                name: "externalroles",
                newName: "catalogs");

            migrationBuilder.AlterColumn<string>(
                name: "name",
                table: "catalogs",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "catalogs",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "prefix",
                table: "catalogs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ResidualLogId",
                table: "catalogs",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_catalogs",
                table: "catalogs",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_catalogs_ResidualLogId",
                table: "catalogs",
                column: "ResidualLogId");

            migrationBuilder.AddForeignKey(
                name: "FK_catalogs_residuallogs_ResidualLogId",
                table: "catalogs",
                column: "ResidualLogId",
                principalTable: "residuallogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_commons_items_in_logs_catalogs_item_id",
                table: "commons_items_in_logs",
                column: "item_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dispatch_items_in_logs_catalogs_ietm_id",
                table: "dispatch_items_in_logs",
                column: "ietm_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dispeser_items_in_logss_catalogs_item_id",
                table: "dispeser_items_in_logss",
                column: "item_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_electric_room_in_logs_catalogs_equip_id",
                table: "electric_room_in_logs",
                column: "equip_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_employees_items_in_logs_catalogs_item_id",
                table: "employees_items_in_logs",
                column: "item_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_equip_logs_catalogs_equip_id",
                table: "equip_logs",
                column: "equip_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_extinguisher_items_in_logs_catalogs_item_id",
                table: "extinguisher_items_in_logs",
                column: "item_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_grocery_items_in_logs_catalogs_item_id",
                table: "grocery_items_in_logs",
                column: "item_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_logs_catalogs_event_type_id",
                table: "logs",
                column: "event_type_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_logs_catalogs_frecuency_id",
                table: "logs",
                column: "frecuency_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_machinery_in_daily_logs_catalogs_machine_id",
                table: "machinery_in_daily_logs",
                column: "machine_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_mens_bathroom_in_logs_catalogs_bathroom_id",
                table: "mens_bathroom_in_logs",
                column: "bathroom_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_residuallogs_catalogs_temp_storage_id",
                table: "residuallogs",
                column: "temp_storage_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_residues_in_logs_catalogs_residue_log",
                table: "residues_in_logs",
                column: "residue_log",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_roles_firms_catalogs_role_id",
                table: "roles_firms",
                column: "role_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_tanks_in_daily_logs_catalogs_tank_id",
                table: "tanks_in_daily_logs",
                column: "tank_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_womens_bathrooms_in_logs_catalogs_bathroom_id",
                table: "womens_bathrooms_in_logs",
                column: "bathroom_id",
                principalTable: "catalogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
