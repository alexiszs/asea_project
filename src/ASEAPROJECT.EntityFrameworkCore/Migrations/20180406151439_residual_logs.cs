﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ASEAPROJECT.Migrations
{
    public partial class residual_logs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ResidualLogId",
                table: "catalogs",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "residuallogs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    certnumber = table.Column<string>(nullable: false),
                    civilpermission = table.Column<string>(nullable: false),
                    emissiondate = table.Column<DateTime>(nullable: false),
                    extraction_firm_id = table.Column<int>(nullable: true),
                    liquidrests = table.Column<int>(nullable: false),
                    manifestnumber = table.Column<string>(nullable: false),
                    personalAuth = table.Column<byte>(nullable: false),
                    reception_firm_id = table.Column<int>(nullable: true),
                    residueType = table.Column<string>(nullable: false),
                    shipping_firm_id = table.Column<int>(nullable: true),
                    solidrests = table.Column<int>(nullable: false),
                    tempsavage = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_residuallogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_residuallogs_external_firms_extraction_firm_id",
                        column: x => x.extraction_firm_id,
                        principalTable: "external_firms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_residuallogs_external_firms_reception_firm_id",
                        column: x => x.reception_firm_id,
                        principalTable: "external_firms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_residuallogs_external_firms_shipping_firm_id",
                        column: x => x.shipping_firm_id,
                        principalTable: "external_firms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "roles_firms",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    firm_id = table.Column<int>(nullable: false),
                    role_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_roles_firms", x => x.Id);
                    table.ForeignKey(
                        name: "FK_roles_firms_external_firms_firm_id",
                        column: x => x.firm_id,
                        principalTable: "external_firms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_roles_firms_catalogs_role_id",
                        column: x => x.role_id,
                        principalTable: "catalogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "equip_logs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    equip_id = table.Column<int>(nullable: false),
                    residual_log_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_equip_logs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_equip_logs_catalogs_equip_id",
                        column: x => x.equip_id,
                        principalTable: "catalogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_equip_logs_residuallogs_residual_log_id",
                        column: x => x.residual_log_id,
                        principalTable: "residuallogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_catalogs_ResidualLogId",
                table: "catalogs",
                column: "ResidualLogId");

            migrationBuilder.CreateIndex(
                name: "IX_equip_logs_equip_id",
                table: "equip_logs",
                column: "equip_id");

            migrationBuilder.CreateIndex(
                name: "IX_equip_logs_residual_log_id",
                table: "equip_logs",
                column: "residual_log_id");

            migrationBuilder.CreateIndex(
                name: "IX_residuallogs_extraction_firm_id",
                table: "residuallogs",
                column: "extraction_firm_id");

            migrationBuilder.CreateIndex(
                name: "IX_residuallogs_reception_firm_id",
                table: "residuallogs",
                column: "reception_firm_id");

            migrationBuilder.CreateIndex(
                name: "IX_residuallogs_shipping_firm_id",
                table: "residuallogs",
                column: "shipping_firm_id");

            migrationBuilder.CreateIndex(
                name: "IX_roles_firms_firm_id",
                table: "roles_firms",
                column: "firm_id");

            migrationBuilder.CreateIndex(
                name: "IX_roles_firms_role_id",
                table: "roles_firms",
                column: "role_id");

            migrationBuilder.AddForeignKey(
                name: "FK_catalogs_residuallogs_ResidualLogId",
                table: "catalogs",
                column: "ResidualLogId",
                principalTable: "residuallogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_catalogs_residuallogs_ResidualLogId",
                table: "catalogs");

            migrationBuilder.DropTable(
                name: "equip_logs");

            migrationBuilder.DropTable(
                name: "roles_firms");

            migrationBuilder.DropTable(
                name: "residuallogs");

            migrationBuilder.DropIndex(
                name: "IX_catalogs_ResidualLogId",
                table: "catalogs");

            migrationBuilder.DropColumn(
                name: "ResidualLogId",
                table: "catalogs");
        }
    }
}
