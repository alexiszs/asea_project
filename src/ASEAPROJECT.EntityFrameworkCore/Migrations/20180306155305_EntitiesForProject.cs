﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ASEAPROJECT.Migrations
{
    public partial class EntitiesForProject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "catalogs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Discriminator = table.Column<string>(nullable: false),
                    name = table.Column<string>(maxLength: 50, nullable: false),
                    prefix = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_catalogs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "external_firms",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    addres = table.Column<string>(maxLength: 300, nullable: false),
                    company_name = table.Column<string>(maxLength: 300, nullable: false),
                    email = table.Column<string>(maxLength: 300, nullable: false),
                    tradename = table.Column<string>(maxLength: 300, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_external_firms", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "logs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    begin = table.Column<DateTime>(nullable: false),
                    certication_number = table.Column<string>(maxLength: 100, nullable: false),
                    comments = table.Column<string>(maxLength: 5000, nullable: true),
                    date = table.Column<DateTime>(nullable: false),
                    end = table.Column<DateTime>(nullable: false),
                    event_type_id = table.Column<int>(nullable: false),
                    firm_id = table.Column<int>(nullable: false),
                    frecuency_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_logs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_logs_catalogs_event_type_id",
                        column: x => x.event_type_id,
                        principalTable: "catalogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_logs_external_firms_firm_id",
                        column: x => x.firm_id,
                        principalTable: "external_firms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_logs_catalogs_frecuency_id",
                        column: x => x.frecuency_id,
                        principalTable: "catalogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_logs_event_type_id",
                table: "logs",
                column: "event_type_id");

            migrationBuilder.CreateIndex(
                name: "IX_logs_firm_id",
                table: "logs",
                column: "firm_id");

            migrationBuilder.CreateIndex(
                name: "IX_logs_frecuency_id",
                table: "logs",
                column: "frecuency_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "logs");

            migrationBuilder.DropTable(
                name: "catalogs");

            migrationBuilder.DropTable(
                name: "external_firms");
        }
    }
}
