using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace ASEAPROJECT.EntityFrameworkCore
{
    public static class ASEAPROJECTDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<ASEAPROJECTDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<ASEAPROJECTDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
