﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using ASEAPROJECT.Configuration;
using ASEAPROJECT.Web;

namespace ASEAPROJECT.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class ASEAPROJECTDbContextFactory : IDesignTimeDbContextFactory<ASEAPROJECTDbContext>
    {
        public ASEAPROJECTDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ASEAPROJECTDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            ASEAPROJECTDbContextConfigurer.Configure(builder, configuration.GetConnectionString(ASEAPROJECTConsts.ConnectionStringName));

            return new ASEAPROJECTDbContext(builder.Options);
        }
    }
}
