﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using ASEAPROJECT.Authorization.Roles;
using ASEAPROJECT.Authorization.Users;
using ASEAPROJECT.MultiTenancy;
using ASEAPROJECT.Entities;

namespace ASEAPROJECT.EntityFrameworkCore
{
    public class ASEAPROJECTDbContext : AbpZeroDbContext<Tenant, Role, User, ASEAPROJECTDbContext>
    {
        /* Define a DbSet for each entity of the application */
        public DbSet<Catalog> Catalog { get; set; }
        public DbSet<Event> Event { get; set; }
        public DbSet<EquipItem> EquipItem { get; set; }
        public DbSet<ExternalRol> ExternalRol { get; set; }
        public DbSet<FrecuencyOfActivity> FrecuencyOfActivity { get; set; }
        public DbSet<ExternalFirm> ExternalFirm { get; set; }
        public DbSet<Log> Log { get; set; }
        public DbSet<CommonItems> CommonItem { get; set; }
        public DbSet<Common_Item_in_log> common_Items { get; set; }
        public DbSet<DangerResidues> Dresidues { get; set; }
        public DbSet<Residues_in_Log> residues_logs { get; set; }
        public DbSet<DispatchItem> dispatchItem { get; set; }
        public DbSet<Dispatch_Item_in_Log> dispatch_Item_In_Logs { get; set; }
        public DbSet<DispenserItem> dispenserItems { get; set; }
        public DbSet<Dispenser_Item_in_Log> dispenser_Item_In_Logs { get; set; }
        public DbSet<ElectricEquip> electricEquips { get; set; }
        public DbSet<Electric_Room_in_Log> electric_Room_In_Logs { get; set; }
        public DbSet<EmployeItem> employeItems { get; set; }
        public DbSet<Employees_Item_in_Log> employees_Item_In_Logs { get; set; }
        public DbSet<ExtinguisherItem> extinguisherItems { get; set; }
        public DbSet<Extinguisher_Item_in_Log> extinguisher_Item_In_Logs { get; set; }
        public DbSet<GroceryItems> groceryItems { get; set; }
        public DbSet<Grocery_Item_in_log> grocery_Item_In_Logs { get; set; }
        public DbSet<Machine> machines { get; set; }
        public DbSet<Machinery_in_Daily_logs> machinery_In_Daily_Logs { get; set; }
        public DbSet<MensBathroom> mensBathrooms { get; set; }
        public DbSet<Mens_Bathroom_in_Log> mens_Bathroom_In_Logs { get; set; }
        public DbSet<Tank> tanks { get; set; }
        public DbSet<Tanks_in_Daily_Logs> tanks_In_Daily_Logs { get; set; }
        public DbSet<WomensBathroom> womensBathrooms { get; set; }
        public DbSet<Womens_Bathroom_in_Log> womens_Bathroom_In_Logs { get; set; }
        public DbSet<ResidualLog> ResidualLog { get; set; }
        public DbSet<RolesFirms> RolesFirms { get; set; }
        public DbSet<EquipLog> EquipLog { get; set; }
        public DbSet<TemporyStorage> TemporyStorage { get; set; }
        public DbSet<DailyVerificationLog> DailyVerificationLog { get; set; }

        public ASEAPROJECTDbContext(DbContextOptions<ASEAPROJECTDbContext> options)
            : base(options)
        {
        }
    }
}
