﻿using ASEAPROJECT.Configuration.Ui;

namespace ASEAPROJECT.Web.Views.Shared.Components.RightSideBar
{
    public class RightSideBarViewModel
    {
        public UiThemeInfo CurrentTheme { get; set; }
    }
}
