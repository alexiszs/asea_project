﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace ASEAPROJECT.Web.Views
{
    public abstract class ASEAPROJECTViewComponent : AbpViewComponent
    {
        protected ASEAPROJECTViewComponent()
        {
            LocalizationSourceName = ASEAPROJECTConsts.LocalizationSourceName;
        }
    }
}
