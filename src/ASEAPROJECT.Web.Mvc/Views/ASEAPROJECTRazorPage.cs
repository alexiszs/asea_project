﻿using Microsoft.AspNetCore.Mvc.Razor.Internal;
using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;

namespace ASEAPROJECT.Web.Views
{
    public abstract class ASEAPROJECTRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected ASEAPROJECTRazorPage()
        {
            LocalizationSourceName = ASEAPROJECTConsts.LocalizationSourceName;
        }
    }
}
