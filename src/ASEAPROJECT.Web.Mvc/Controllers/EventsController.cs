﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Extensions;
using ASEAPROJECT.Controllers;
using ASEAPROJECT.Dtos.Events;
using ASEAPROJECT.Services.EventService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASEAPROJECT.Web.Mvc.Controllers
{
    public class EventsController : ASEAPROJECTControllerBase
    {

        private readonly IEventService _eventService;

        public EventsController(IEventService eventService)
        {
            _eventService = eventService;
        }

        public async Task<IActionResult> Index()
        {
            var events = (await _eventService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            return View(events);
        }


        public IActionResult Create()
        {
            if (Request.IsAjaxRequest())
                return PartialView("Create");
            else
                return View("Create");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(EventDto eventDto)
        {
            if(ModelState.ErrorCount == 0)
            {
                await _eventService.Create(eventDto);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }
            if (Request.IsAjaxRequest())
                return PartialView("Create", eventDto);
            else
                return View("Create", eventDto);
        }

        public async Task<IActionResult> Edit(int id)
        {
            var model = await _eventService.Get(id);
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit( EventDto eventDto)
        {
            if (ModelState.ErrorCount == 0)
            {
                await _eventService.Update(eventDto);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }

            if (Request.IsAjaxRequest())
                return PartialView("Edit", eventDto);
            else
                return View("Edit", eventDto);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            var model = await _eventService.Get(id);
            var result = _eventService.Delete(model);

            return new JsonResult(new { success = true, data = result });
        }
    }
}