﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ASEAPROJECT.Controllers;
using ASEAPROJECT.Services.TemporqaryStorageService;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Extensions;
using ASEAPROJECT.Dtos.TemporyStorages;

namespace ASEAPROJECT.Web.Mvc.Controllers
{
    public class TemporaryStorageController : ASEAPROJECTControllerBase
    {
        private readonly ITemporaryStorageService _temporaryStorageService;
        public TemporaryStorageController(ITemporaryStorageService temporaryStorageService)
        {
            _temporaryStorageService = temporaryStorageService;
        }
        // GET: TemporaryStorage
        public async Task<IActionResult> Index()
        {
            var storages = (await _temporaryStorageService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            return View(storages);
        }


        // GET: TemporaryStorage/Create
        public IActionResult Create()
        {
            if (Request.IsAjaxRequest())
                return PartialView("Create");
            else
                return View("Create");
        }

        // POST: TemporaryStorage/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(TemporyStorageDto storage)
        {
            if (ModelState.ErrorCount == 0)
            {
                await _temporaryStorageService.Create(storage);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }
            if (Request.IsAjaxRequest())
                return PartialView("Create", storage);
            else
                return View("Create", storage);
        }

        // GET: TemporaryStorage/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            var model = await _temporaryStorageService.Get(id);
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        // POST: TemporaryStorage/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(TemporyStorageDto storage)
        {
            if (ModelState.ErrorCount == 0)
            {
                await _temporaryStorageService.Update(storage);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }

            if (Request.IsAjaxRequest())
                return PartialView("Edit", storage);
            else
                return View("Edit", storage);
        }

        // POST: TemporaryStorage/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            var model = await _temporaryStorageService.Get(id);
            var result = _temporaryStorageService.Delete(model);

            return new JsonResult(new { success = true, data = result });
        }
    }
}