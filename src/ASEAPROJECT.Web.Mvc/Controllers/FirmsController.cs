﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ASEAPROJECT.Controllers;
using ASEAPROJECT.Services.ExternalFirmService;
using Abp.AspNetCore.Mvc.Extensions;
using ASEAPROJECT.Dtos.ExternalFirms;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Services.ExternalRoleService;
using ASEAPROJECT.Entities;
using ASEAPROJECT.Services.RolesFirmService;
using ASEAPROJECT.Dtos.RolesFirm;
using AutoMapper;

namespace ASEAPROJECT.Web.Mvc.Controllers
{
    public class FirmsController : ASEAPROJECTControllerBase
    {
        private readonly IExternalFirmService _firmService;
        private readonly IExternalRoleService _rolesService;
        private readonly IRolesFirmService _rolesFirmsService;

        public FirmsController(IExternalFirmService firmService, IRolesFirmService rolesFirmService, IExternalRoleService roleService)
        {
            _firmService = firmService;
            _rolesService = roleService;
            _rolesFirmsService = rolesFirmService;
        }

        // GET: Event
        public async Task<IActionResult> Index()
        {
            var events = (await _firmService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            return View(events);
        }


        // GET: Event/Create
        public async Task<IActionResult> Create()
        {
            ViewBag.roles = (await _rolesService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            var model = new ExternalFirmDto();
            model.roles = new List<RolesFirms>();
            if (Request.IsAjaxRequest())
                return PartialView("Create",model);
            else
                return View("Create");
        }

        // POST: Event/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ExternalFirmDto ExternalFirmDto, List<int> roles = null)
        {
            if (ModelState.ErrorCount == 0)
            {
                ExternalFirmDto.roles = new List<RolesFirms>();
                foreach(var role in roles)
                {
                    ExternalFirmDto.roles.Add(new RolesFirms() { firm_id = ExternalFirmDto.Id, role_id = role });
                }
                await _firmService.Create(ExternalFirmDto);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }
            if (Request.IsAjaxRequest())
                return PartialView("Create", ExternalFirmDto);
            else
                return View("Create", ExternalFirmDto);
        }

        // GET: Event/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            ViewBag.roles = (await _rolesService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            var model = await _firmService.Get(id);
            model.roles = Mapper.Map<List< RolesFirms>>(_rolesFirmsService.GetAllByFirm(id));
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        // POST: Event/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ExternalFirmDto firmDto, List<int> roles = null)
        {
            if (ModelState.ErrorCount == 0)
            {
                firmDto.roles = new List<RolesFirms>();
                foreach (var role in roles)
                {
                    firmDto.roles.Add(new RolesFirms() { firm_id = firmDto.Id, role_id = role });
                }
                await _firmService.Update(firmDto);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }

            if (Request.IsAjaxRequest())
                return PartialView("Edit", firmDto);
            else
                return View("Edit", firmDto);
        }

        // POST: Event/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            var model = await _firmService.Get(id);
            var result = _firmService.Delete(model);

            return new JsonResult(new { success = true, data = result });
        }
    }
}