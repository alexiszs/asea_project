﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ASEAPROJECT.Services.DispatchItemService;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Extensions;
using ASEAPROJECT.Dtos.DispatchItems;
using ASEAPROJECT.Controllers;

namespace ASEAPROJECT.Web.Mvc.Controllers
{
    public class DispatchItemsController : ASEAPROJECTControllerBase
    {
        private readonly IDispatchItemService _itemsService;

        public DispatchItemsController( IDispatchItemService itemService)
        {
            _itemsService = itemService;
        }

        // GET: DispatchItems
        public async Task<IActionResult> Index()
        {
            var items = (await _itemsService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            return View(items);
        }


        // GET: DispatchItems/Create
        public IActionResult Create()
        {
            if (Request.IsAjaxRequest())
                return PartialView("Create");
            else
                return View("Create");
        }

        // POST: DispatchItems/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(DispatchItemDto item)
        {
            if (ModelState.ErrorCount == 0)
            {
                await _itemsService.Create(item);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }
            if (Request.IsAjaxRequest())
                return PartialView("Create", item);
            else
                return View("Create", item);
        }

        // GET: DispatchItems/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            var model = await _itemsService.Get(id);
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        // POST: DispatchItems/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(DispatchItemDto item)
        {
            if (ModelState.ErrorCount == 0)
            {
                await _itemsService.Update(item);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }

            if (Request.IsAjaxRequest())
                return PartialView("Edit", item);
            else
                return View("Edit", item);
        }

        // POST: DispatchItems/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            var model = await _itemsService.Get(id);
            var result = _itemsService.Delete(model);

            return new JsonResult(new { success = true, data = result });
        }
    }
}