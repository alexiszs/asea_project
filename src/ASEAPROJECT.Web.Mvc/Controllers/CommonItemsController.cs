﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Extensions;
using ASEAPROJECT.Controllers;
using ASEAPROJECT.Dtos.CommonItemDto;
using ASEAPROJECT.Services.CommonItemService;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ASEAPROJECT.Web.Mvc.Controllers
{
    public class CommonItemsController : ASEAPROJECTControllerBase
    {
        private readonly ICommonItemService _itemsService;

        public CommonItemsController(ICommonItemService itemsService)
        {
            _itemsService = itemsService;
        }

        public async Task<IActionResult> Index()
        {
            var items = (await _itemsService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            return View(items);
        }


        public IActionResult Create()
        {
            if (Request.IsAjaxRequest())
                return PartialView("Create");
            else
                return View("Create");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CommonItemsDto item)
        {
            if (ModelState.ErrorCount == 0)
            {
                await _itemsService.Create(item);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }
            if (Request.IsAjaxRequest())
                return PartialView("Create", item);
            else
                return View("Create", item);
        }

        public async Task<IActionResult> Edit(int id)
        {
            var model = await _itemsService.Get(id);
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(CommonItemsDto item)
        {
            if (ModelState.ErrorCount == 0)
            {
                await _itemsService.Update(item);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }

            if (Request.IsAjaxRequest())
                return PartialView("Edit", item);
            else
                return View("Edit", item);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            var model = await _itemsService.Get(id);
            var result = _itemsService.Delete(model);

            return new JsonResult(new { success = true, data = result });
        }
    }
}