﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ASEAPROJECT.Services.DangerResidueService;
using Abp.AspNetCore.Mvc.Extensions;
using ASEAPROJECT.Dtos.DangerResidue;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Controllers;

namespace ASEAPROJECT.Web.Mvc.Controllers
{
    public class DangerResiduesController : ASEAPROJECTControllerBase
    {
        private readonly IDangerResidueService _residuesService;

        public DangerResiduesController(IDangerResidueService residueService)
        {
            _residuesService = residueService;
        }
        // GET: DangerResidues
        public async Task<IActionResult> Index()
        {
            var residues =  (await _residuesService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            return View(residues);
        }

        // GET: DangerResidues/Create
        public IActionResult Create()
        {
            if (Request.IsAjaxRequest())
                return PartialView("Create");
            else
                return View("Create");
        }

        // POST: DangerResidues/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(DangerResidueDto residue)
        {
            if(ModelState.ErrorCount == 0)
            {
                await _residuesService.Create(residue);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }
            if (Request.IsAjaxRequest())
                return PartialView("Create", residue);
            else
                return View("Create", residue);
        }

        // GET: DangerResidues/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            var model = await _residuesService.Get(id);
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        // POST: DangerResidues/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(DangerResidueDto residue)
        {

            if (ModelState.ErrorCount == 0)
            {
                await _residuesService.Update(residue);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }

            if (Request.IsAjaxRequest())
                return PartialView("Edit", residue);
            else
                return View("Edit", residue);
        }

  

        // POST: DangerResidues/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            var model = await _residuesService.Get(id);
            var result = _residuesService.Delete(model);

            return new JsonResult(new { success = true, data = result });
        }
    }
}