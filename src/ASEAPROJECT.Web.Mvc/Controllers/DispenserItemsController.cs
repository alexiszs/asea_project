﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ASEAPROJECT.Services.DispenserItemService;
using ASEAPROJECT.Dtos.DispenserItems;
using Abp.AspNetCore.Mvc.Extensions;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Controllers;

namespace ASEAPROJECT.Web.Mvc.Controllers
{
    public class DispenserItemsController : ASEAPROJECTControllerBase
    {
        private readonly IDispenserItemService _itemsService;

        public DispenserItemsController(IDispenserItemService itemService) {
            _itemsService = itemService;
        }

        // GET: DispenserItems
        public async Task<IActionResult> Index()
        {
            var events = (await _itemsService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            return View(events);
        }


        // GET: DispenserItems/Create
        public IActionResult Create()
        {
            if (Request.IsAjaxRequest())
                return PartialView("Create");
            else
                return View("Create");
        }

        // POST: DispenserItems/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(DispenserItemDto item)
        {
            if (ModelState.ErrorCount == 0)
            {
                await _itemsService.Create(item);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }
            if (Request.IsAjaxRequest())
                return PartialView("Create", item);
            else
                return View("Create", item);
        }

        // GET: DispenserItems/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            var model = await _itemsService.Get(id);
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        // POST: DispenserItems/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(DispenserItemDto item)
        {
            if (ModelState.ErrorCount == 0)
            {
                await _itemsService.Update(item);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }

            if (Request.IsAjaxRequest())
                return PartialView("Edit", item);
            else
                return View("Edit", item);
        }


        // POST: DispenserItems/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            var model = await _itemsService.Get(id);
            var result = _itemsService.Delete(model);

            return new JsonResult(new { success = true, data = result });
        }
    }
}