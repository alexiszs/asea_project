﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ASEAPROJECT.Controllers;
using ASEAPROJECT.Services.FrecuencyOfActivityService;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Extensions;
using ASEAPROJECT.Dtos.FrecuencyOfActivities;

namespace ASEAPROJECT.Web.Mvc.Controllers
{
    public class FrecuenciesController : ASEAPROJECTControllerBase
    {
        private readonly IFrecuencyOfActivityService _frecuencyService;

        public FrecuenciesController(IFrecuencyOfActivityService frecuencyService)
        {
            _frecuencyService = frecuencyService;
        }

        // GET: Frecuencies
        public async Task<IActionResult> Index()
        {
            var events = (await _frecuencyService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            return View(events);
        }

        // GET: Frecuencies/Create
        public IActionResult Create()
        {
            if (Request.IsAjaxRequest())
                return PartialView("Create");
            else
                return View("Create");
        }

        // POST: Frecuencies/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(FrecuencyOfActivityDto frecuencyDto)
        {
            if (ModelState.ErrorCount == 0)
            {
                await _frecuencyService.Create(frecuencyDto);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }
            if (Request.IsAjaxRequest())
                return PartialView("Create", frecuencyDto);
            else
                return View("Create", frecuencyDto);
        }

        // GET: Frecuencies/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            var model = await _frecuencyService.Get(id);
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        // POST: Frecuencies/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(FrecuencyOfActivityDto frecuencyDto)
        {
            if (ModelState.ErrorCount == 0)
            {
                await _frecuencyService.Update(frecuencyDto);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }

            if (Request.IsAjaxRequest())
                return PartialView("Edit", frecuencyDto);
            else
                return View("Edit", frecuencyDto);
        }


        // POST: Frecuencies/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            var model = await _frecuencyService.Get(id);
            var result = _frecuencyService.Delete(model);

            return new JsonResult(new { success = true, data = result });
        }
    }
}