﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ASEAPROJECT.Controllers;
using ASEAPROJECT.Services.ExternalRoleService;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Extensions;
using ASEAPROJECT.Dtos.ExternalRoles;

namespace ASEAPROJECT.Web.Mvc.Controllers
{
    public class ExternalRolesController : ASEAPROJECTControllerBase
    {
        private readonly IExternalRoleService _rolesService;

        public ExternalRolesController(IExternalRoleService roleService)
        {
            _rolesService = roleService;
        }

        // GET: ExternalRoles
        public async Task<IActionResult> Index()
        {
            var roles = (await _rolesService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            return View(roles);
        }


        // GET: ExternalRoles/Create
        public IActionResult Create()
        {
            if (Request.IsAjaxRequest())
                return PartialView("Create");
            else
                return View("Create");
        }

        // POST: ExternalRoles/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ExternalRoleDto role)
        {
            if (ModelState.ErrorCount == 0)
            {
                await _rolesService.Create(role);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }
            if (Request.IsAjaxRequest())
                return PartialView("Create", role);
            else
                return View("Create", role);
        }

        // GET: ExternalRoles/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            var model = await _rolesService.Get(id);
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        // POST: ExternalRoles/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ExternalRoleDto role)
        {
            if (ModelState.ErrorCount == 0)
            {
                await _rolesService.Update(role);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }

            if (Request.IsAjaxRequest())
                return PartialView("Edit", role);
            else
                return View("Edit", role);
        }


        // POST: ExternalRoles/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            var model = await _rolesService.Get(id);
            var result = _rolesService.Delete(model);

            return new JsonResult(new { success = true, data = result });
        }
    }
}