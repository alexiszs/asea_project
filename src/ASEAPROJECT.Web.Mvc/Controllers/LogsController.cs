﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ASEAPROJECT.Services.LogService;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Extensions;
using ASEAPROJECT.Dtos.Logs;
using ASEAPROJECT.Controllers;
using ASEAPROJECT.Services.SelectListService;
using Microsoft.AspNetCore.Mvc.Rendering;
using ASEAPROJECT.Services.ExternalFirmService;
using ASEAPROJECT.Services.FrecuencyOfActivityService;
using ASEAPROJECT.Services.EventService;
using Rotativa.AspNetCore;

namespace ASEAPROJECT.Web.Mvc.Controllers
{
    public class LogsController : ASEAPROJECTControllerBase
    {
        private readonly ILogService _logService;
        private readonly IExternalFirmService _firmsService;
        private readonly IFrecuencyOfActivityService _frecuenciesService;
        private readonly IEventService _eventService;

        public LogsController(ILogService logService, IExternalFirmService firmsService, IFrecuencyOfActivityService frecuenciesService, IEventService eventService)
        {
            _logService = logService;
            _firmsService = firmsService;
            _frecuenciesService = frecuenciesService;
            _eventService = eventService;
        }


        // GET: Logs
        public async Task<IActionResult> Index()
        {
            var logs = (await _logService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            return View(logs);
        }


        // GET: Logs/Create
        public async Task<IActionResult> Create()
        {
            ViewBag.model = new LogDto();
            await setViewBag();
            return View();
        }

        private async Task setViewBag()
        {

            ViewBag.firms = (await _firmsService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            ViewBag.events = (await _eventService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            ViewBag.frecuencies = (await _frecuenciesService.GetAll(new PagedAndSortedResultRequestDto())).Items;
        }

        // POST: Logs/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(LogDto logDto)
        {
            if (ModelState.ErrorCount == 0)
            {
                await _logService.Create(logDto);
                return RedirectToAction("Index");
            }
                return View("Create", logDto);
        }

        // GET: Logs/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            await setViewBag();
            var model = await _logService.Get(id);
            return View(model);
        }

        // POST: Logs/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(LogDto logDto)
        {
            if (ModelState.ErrorCount == 0)
            {
                await _logService.Update(logDto);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }

            if (Request.IsAjaxRequest())
                return PartialView("Edit", logDto);
            else
                return View("Edit", logDto);
        }


        // POST: Logs/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            var model = await _logService.Get(id);
            var result = _logService.Delete(model);

            return new JsonResult(new { success = true, data = result });
        }

        public async Task<IActionResult> toPdf(int id)
        {
            var model = await _logService.Get(id);
            return new ViewAsPdf("LogAsPdf",model);
        }
    }
}