﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Extensions;
using ASEAPROJECT.Controllers;
using ASEAPROJECT.Dtos.GroceryItem;
using ASEAPROJECT.Services.GroceryItemService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ASEAPROJECT.Web.Mvc.Controllers
{
    public class GroceryItemsController : ASEAPROJECTControllerBase
    {
        private readonly IGroceryItemService _itemService;

        public GroceryItemsController(IGroceryItemService itemService)
        {
            _itemService = itemService;
        }

        public async Task<IActionResult> Index()
        {
            var items = (await _itemService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            return View(items);
        }


        public IActionResult Create()
        {
            if (Request.IsAjaxRequest())
                return PartialView("Create");
            else
                return View("Create");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GroceryItemsDto item)
        {
            if (ModelState.ErrorCount == 0)
            {
                await _itemService.Create(item);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }
            if (Request.IsAjaxRequest())
                return PartialView("Create", item);
            else
                return View("Create", item);
        }

        public async Task<IActionResult> Edit(int id)
        {
            var model = await _itemService.Get(id);
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GroceryItemsDto item)
        {
            if (ModelState.ErrorCount == 0)
            {
                await _itemService.Update(item);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }

            if (Request.IsAjaxRequest())
                return PartialView("Edit", item);
            else
                return View("Edit", item);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            var model = await _itemService.Get(id);
            var result = _itemService.Delete(model);

            return new JsonResult(new { success = true, data = result });
        }
    }
}