﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using ASEAPROJECT.Controllers;

namespace ASEAPROJECT.Web.Controllers
{
    [AbpMvcAuthorize]
    public class AboutController : ASEAPROJECTControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}
