﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Extensions;
using ASEAPROJECT.Controllers;
using ASEAPROJECT.Dtos.Common_item_inLogs;
using ASEAPROJECT.Dtos.CommonItemDto;
using ASEAPROJECT.Dtos.DailyVerificationLogs;
using ASEAPROJECT.Dtos.Dispatch_Item_in_Logs;
using ASEAPROJECT.Dtos.Dispenser_Item_in_Logs;
using ASEAPROJECT.Dtos.Electric_Room_in_logs;
using ASEAPROJECT.Dtos.Employees_item_in_logs;
using ASEAPROJECT.Dtos.Extinguisher_Item_in_Logs;
using ASEAPROJECT.Dtos.Grocery_Item_in_Logs;
using ASEAPROJECT.Dtos.Machinery_in_Daily_log;
using ASEAPROJECT.Dtos.Mens_Bathroom_In_Logs;
using ASEAPROJECT.Dtos.Residues_in_Logs;
using ASEAPROJECT.Dtos.Tanks_in_Daily_logs;
using ASEAPROJECT.Dtos.Womens_Bathroom_in_Logs;
using ASEAPROJECT.Services.CommonItemService;
using ASEAPROJECT.Services.DailyVerificationLogService;
using ASEAPROJECT.Services.DangerResidueService;
using ASEAPROJECT.Services.DispatchItemService;
using ASEAPROJECT.Services.DispenserItemService;
using ASEAPROJECT.Services.ElectricEquipService;
using ASEAPROJECT.Services.EmployeItemService;
using ASEAPROJECT.Services.ExtinguisherItemService;
using ASEAPROJECT.Services.GroceryItemService;
using ASEAPROJECT.Services.MachineService;
using ASEAPROJECT.Services.MensBathroomService;
using ASEAPROJECT.Services.TankService;
using ASEAPROJECT.Services.WomensBathroomService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Rotativa.AspNetCore;

namespace ASEAPROJECT.Web.Mvc.Controllers
{
    public class DailyLogsController : ASEAPROJECTControllerBase
    {
        private readonly IDailyVerificationService _logsService;
        private readonly ITankService _tankService;
        private readonly IMachineService _machineService;
        private readonly IDangerResidueService _residuesService;
        private readonly IMensBathroomService _mensService;
        private readonly IWomensBathroomService _womenService;
        private readonly IElectricEquipService _electricService;
        private readonly IGroceryItemService _groceryService;
        private readonly ICommonItemService _commonService;
        private readonly IDispenserItemService _dispenserService;
        private readonly IDispatchItemService _dispatchService;
        private readonly IEmployeItemService _employeeService;
        private readonly IExtinguisherItemService _extinguisherService;

        public DailyLogsController(IDailyVerificationService logsService, ITankService tankService,
            IMachineService machineService, IDangerResidueService residueService, IMensBathroomService mensService,
            IWomensBathroomService womenService, IElectricEquipService electricService, IGroceryItemService groceryService,
            ICommonItemService commonService, IDispenserItemService dispenserService, IDispatchItemService dispatchService,
            IEmployeItemService employeService, IExtinguisherItemService extinguisherService)
        {
            _logsService = logsService;
            _tankService = tankService;
            _machineService = machineService;
            _residuesService = residueService;
            _mensService = mensService;
            _womenService = womenService;
            _electricService = electricService;
            _groceryService = groceryService;
            _commonService = commonService;
            _dispenserService = dispenserService;
            _dispatchService = dispatchService;
            _employeeService = employeService;
            _extinguisherService = extinguisherService;
        }

        // GET: Logs
        public async Task<IActionResult> Index()
        {
            var logs = (await _logsService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            return View(logs);
        }


        // GET: Logs/Create
        public async Task<IActionResult> Create()
        {
            var model = new DailyVerifcationLogDto();
            model.common = new List<Common_item_in_log_Dto>();
            model.tanks = new List<Tanks_in_Daily_Logs_Dto>();
            model.machinary = new List<Machinery_in_Daily_logs_Dto>();
            model.residues = new List<Residues_in_log_Dto>();
            model.Mbathroom = new List<Mens_bathroom_In_Log_Dto>();
            model.Wbathroom = new List<Womens_Bathroom_in_Log_Dto>();
            model.grocery = new List<Grocery_Item_in_Log_Dto>();
            model.ElectrictRoom = new List<Electric_Room_in_Log_Dto>();
            model.dispatch = new List<Dispatch_Item_in_Log_Dto>();
            model.dispenser = new List<Dispenser_Item_in_Log_Dto>();
            model.employees = new List<Employees_Item_in_logs_Dto>();
            model.extinguisher = new List<Extinguisher_Item_in_Log_Dto>();
            return View(model);
        }

        private async Task setViewBag()
        {
            ViewBag.tanks = (await _tankService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            ViewBag.machines = (await _machineService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            ViewBag.residues = (await _residuesService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            ViewBag.mens = (await _mensService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            ViewBag.women = (await _womenService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            ViewBag.electrics = (await _electricService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            ViewBag.grocery = (await _groceryService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            ViewBag.commons = (await _commonService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            ViewBag.dispensers = (await _dispenserService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            ViewBag.dispatchers = (await _dispatchService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            ViewBag.employees = (await _employeeService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            ViewBag.extinguishers = (await _extinguisherService.GetAll(new PagedAndSortedResultRequestDto())).Items;
        }

        // POST: Logs/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(DailyVerifcationLogDto logDto, List<int> tanks = null, List<int> machines = null,
            List<int> residues = null, List<int> mens = null, List<int> women = null, List<int> electrics = null, List<int> grocery = null,
            List<int> commons = null, List<int> dispensers = null, List<int> dispatchers = null, List<int> employees = null, List<int> extinguishers = null)
        {
            if (ModelState.ErrorCount == 0)
            {
                logDto.common = new List<Common_item_in_log_Dto>();
                logDto.common = commons.Select(i => new Common_item_in_log_Dto() { log_id = logDto.Id, item_id = i }).ToList();
                logDto.tanks = new List<Tanks_in_Daily_Logs_Dto>();
                logDto.tanks = tanks.Select(i => new Tanks_in_Daily_Logs_Dto() { log_id = logDto.Id, tank_id = i }).ToList();
                logDto.machinary = new List<Machinery_in_Daily_logs_Dto>();
                logDto.machinary = machines.Select(i => new Machinery_in_Daily_logs_Dto() { log_id = logDto.Id, machine_id = i }).ToList();
                logDto.residues = new List<Residues_in_log_Dto>();
                logDto.residues = residues.Select(i => new Residues_in_log_Dto() { log_id = logDto.Id, residue_id = i }).ToList();
                logDto.Mbathroom = new List<Mens_bathroom_In_Log_Dto>();
                logDto.Mbathroom = mens.Select(i => new Mens_bathroom_In_Log_Dto() { log_id = logDto.Id, bathroom_id = i }).ToList();
                logDto.Wbathroom = new List<Womens_Bathroom_in_Log_Dto>();
                logDto.Wbathroom = women.Select(i => new Womens_Bathroom_in_Log_Dto() { log_id = logDto.Id, bathroom_id = i }).ToList();
                logDto.grocery = new List<Grocery_Item_in_Log_Dto>();
                logDto.grocery = grocery.Select(i => new Grocery_Item_in_Log_Dto() { log_id = logDto.Id, item_id = i }).ToList();
                logDto.ElectrictRoom = new List<Electric_Room_in_Log_Dto>();
                logDto.ElectrictRoom = electrics.Select(i => new Electric_Room_in_Log_Dto() { log_id = logDto.Id, equip_id = i }).ToList();
                logDto.dispatch = new List<Dispatch_Item_in_Log_Dto>();
                logDto.dispatch = dispatchers.Select(i => new Dispatch_Item_in_Log_Dto() { log_id = logDto.Id, item_id = i }).ToList();
                logDto.dispenser = new List<Dispenser_Item_in_Log_Dto>();
                logDto.dispenser = dispensers.Select(i => new Dispenser_Item_in_Log_Dto() { log_id = logDto.Id, item_id = i }).ToList();
                logDto.employees = new List<Employees_Item_in_logs_Dto>();
                logDto.employees = employees.Select(i => new Employees_Item_in_logs_Dto() { log_id = logDto.Id, item_id = i }).ToList();
                logDto.extinguisher = new List<Extinguisher_Item_in_Log_Dto>();
                logDto.extinguisher = extinguishers.Select(i => new Extinguisher_Item_in_Log_Dto() { log_id = logDto.Id, item_id = i }).ToList();
                await _logsService.Create(logDto);
                return RedirectToAction("Index");
            }
            return View("Create", logDto);
        }

        // GET: Logs/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            //await setViewBag();
            var model = await _logsService.Get(id);
            return View(model);
        }

        // POST: Logs/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(DailyVerifcationLogDto logDto, List<int> tanks = null, List<int> machines = null,
            List<int> residues = null, List<int> mens = null, List<int> women = null, List<int> electrics = null, List<int> grocery = null,
            List<int> commons = null, List<int> dispensers = null, List<int> dispatchers = null, List<int> employees = null, List<int> extinguishers = null)
        {
            if (ModelState.ErrorCount == 0)
            {
                logDto.common = new List<Common_item_in_log_Dto>();
                logDto.common = commons.Select(i => new Common_item_in_log_Dto() { log_id = logDto.Id, item_id = i }).ToList();
                logDto.tanks = new List<Tanks_in_Daily_Logs_Dto>();
                logDto.tanks = tanks.Select(i => new Tanks_in_Daily_Logs_Dto() { log_id = logDto.Id, tank_id = i }).ToList();
                logDto.machinary = new List<Machinery_in_Daily_logs_Dto>();
                logDto.machinary = machines.Select(i => new Machinery_in_Daily_logs_Dto() { log_id = logDto.Id, machine_id = i }).ToList();
                logDto.residues = new List<Residues_in_log_Dto>();
                logDto.residues = residues.Select(i => new Residues_in_log_Dto() { log_id = logDto.Id, residue_id = i }).ToList();
                logDto.Mbathroom = new List<Mens_bathroom_In_Log_Dto>();
                logDto.Mbathroom = mens.Select(i => new Mens_bathroom_In_Log_Dto() { log_id = logDto.Id, bathroom_id = i }).ToList();
                logDto.Wbathroom = new List<Womens_Bathroom_in_Log_Dto>();
                logDto.Wbathroom = women.Select(i => new Womens_Bathroom_in_Log_Dto() { log_id = logDto.Id, bathroom_id = i }).ToList();
                logDto.grocery = new List<Grocery_Item_in_Log_Dto>();
                logDto.grocery = grocery.Select(i => new Grocery_Item_in_Log_Dto() { log_id = logDto.Id, item_id = i }).ToList();
                logDto.ElectrictRoom = new List<Electric_Room_in_Log_Dto>();
                logDto.ElectrictRoom = electrics.Select(i => new Electric_Room_in_Log_Dto() { log_id = logDto.Id, equip_id = i }).ToList();
                logDto.dispatch = new List<Dispatch_Item_in_Log_Dto>();
                logDto.dispatch = dispatchers.Select(i => new Dispatch_Item_in_Log_Dto() { log_id = logDto.Id, item_id = i }).ToList();
                logDto.dispenser = new List<Dispenser_Item_in_Log_Dto>();
                logDto.dispenser = dispensers.Select(i => new Dispenser_Item_in_Log_Dto() { log_id = logDto.Id, item_id = i }).ToList();
                logDto.employees = new List<Employees_Item_in_logs_Dto>();
                logDto.employees = employees.Select(i => new Employees_Item_in_logs_Dto() { log_id = logDto.Id, item_id = i }).ToList();
                logDto.extinguisher = new List<Extinguisher_Item_in_Log_Dto>();
                logDto.extinguisher = extinguishers.Select(i => new Extinguisher_Item_in_Log_Dto() { log_id = logDto.Id, item_id = i }).ToList();
                await _logsService.Update(logDto);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }

            if (Request.IsAjaxRequest())
                return PartialView("Edit", logDto);
            else
                return View("Edit", logDto);
        }


        // POST: Logs/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            var model = await _logsService.Get(id);
            var result = _logsService.Delete(model);

            return new JsonResult(new { success = true, data = result });
        }

        public async Task<IActionResult> toPdf(int id)
        {
            var model = await _logsService.Get(id);
            return new ViewAsPdf("LogAsPdf", model);
        }
    }
}