﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ASEAPROJECT.Controllers;
using ASEAPROJECT.Services.ResidualLogService;
using ASEAPROJECT.Services.ExternalFirmService;
using ASEAPROJECT.Services.EquipItemService;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.ResidualLogs;
using Abp.AspNetCore.Mvc.Extensions;
using Rotativa.AspNetCore;
using System.Collections.Generic;
using ASEAPROJECT.Entities;
using AutoMapper;
using ASEAPROJECT.Services.EquipLogService;
using ASEAPROJECT.Services.TemporqaryStorageService;

namespace ASEAPROJECT.Web.Mvc.Controllers
{
    public class ResidualLogsController : ASEAPROJECTControllerBase
    {
        private readonly IResidualLogService _residualLogService;
        private readonly IExternalFirmService _externalFirmService;
        private readonly IEquipItemService _equipItemService;
        private readonly IEquipLogService _equipLogService;
        private readonly ITemporaryStorageService _temporaryStorageService;


        public ResidualLogsController(IResidualLogService residualLogService,
            IExternalFirmService externalFirmService, IEquipItemService equipItemService,
            IEquipLogService equipLogService, ITemporaryStorageService temporaryStorageService)
        {
            _residualLogService = residualLogService;
            _externalFirmService = externalFirmService;
            _equipItemService = equipItemService;
            _equipLogService = equipLogService;
            _temporaryStorageService = temporaryStorageService;
        }


        // GET: ResidualLogs
        public async Task<IActionResult> Index()
        {
            var logs = (await _residualLogService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            return View(logs);
        }


        // GET: ResidualLogs/Create
        public async Task<IActionResult> Create()
        {
            var model = new ResidualLogDto();
            model.equipment = new List<EquipLog>();
            await setViewBag();
            return View(model);
        }

        private async Task setViewBag()
        {
            ViewBag.items = (await _equipItemService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            ViewBag.extraccion = (await _externalFirmService.GetAllwithroles()).Where(r=> r.roles.Any(e => e.role.name == "Extraccion"));
            ViewBag.transporte = (await _externalFirmService.GetAllwithroles()).Where(r => r.roles.Any(e => e.role.name == "Transporte"));
            ViewBag.Recepcion = (await _externalFirmService.GetAllwithroles()).Where(r => r.roles.Any(e => e.role.name == "Recepcion"));
            ViewBag.storages = (await _externalFirmService.GetAllwithroles()).Where(r => r.roles.Any(e => e.role.name == "Almacenamiento"));
        }

        // POST: ResidualLogs/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ResidualLogDto log, List<int> equipment = null)
        {
            if (ModelState.ErrorCount == 0)
            {
                log.equipment = new List<EquipLog>();
                foreach(var equip in equipment)
                {
                    log.equipment.Add(new EquipLog() { residual_log_id = log.Id, equip_id = equip });
                }
                await _residualLogService.Create(log);
                return RedirectToAction("Index");
            }
            return View("Create", log);
        }

        // GET: ResidualLogs/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            await setViewBag();
            var model = await _residualLogService.Get(id);
            model.equipment = Mapper.Map<List<EquipLog>>(_equipLogService.GetAllByLog(id));
            return View(model);
        }

        // POST: ResidualLogs/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ResidualLogDto log, List<int> equipment = null)
        {
            if (ModelState.ErrorCount == 0)
            {
                log.equipment = new List<EquipLog>();
                foreach (var equip in equipment)
                {
                    log.equipment.Add(new EquipLog() { residual_log_id = log.Id, equip_id = equip });
                }
                await _residualLogService.Update(log);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }

            if (Request.IsAjaxRequest())
                return PartialView("Edit", log);
            else
                return View("Edit", log);
        }

        // POST: ResidualLogs/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            var model = await _residualLogService.Get(id);
            var result = _residualLogService.Delete(model);

            return new JsonResult(new { success = true, data = result });
        }

        public async Task<IActionResult> toPdf(int id)
        {
            var model = await _residualLogService.Get(id);
            return new ViewAsPdf("ResidualAsPdf",model);
        }
    }
}