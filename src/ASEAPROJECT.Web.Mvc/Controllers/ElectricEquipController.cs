﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ASEAPROJECT.Services.ElectricEquipService;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Extensions;
using ASEAPROJECT.Dtos.ElectricEquips;
using ASEAPROJECT.Controllers;

namespace ASEAPROJECT.Web.Mvc.Controllers
{
    public class ElectricEquipController : ASEAPROJECTControllerBase
    {
        private readonly IElectricEquipService _equipService;

        public ElectricEquipController(IElectricEquipService equipService)
        {
            _equipService = equipService;
        }

        public async Task<IActionResult> Index()
        {
            var events = (await _equipService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            return View(events);
        }


        public IActionResult Create()
        {
            if (Request.IsAjaxRequest())
                return PartialView("Create");
            else
                return View("Create");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ElectricEquipDto eventDto)
        {
            if (ModelState.ErrorCount == 0)
            {
                await _equipService.Create(eventDto);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }
            if (Request.IsAjaxRequest())
                return PartialView("Create", eventDto);
            else
                return View("Create", eventDto);
        }

        public async Task<IActionResult> Edit(int id)
        {
            var model = await _equipService.Get(id);
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ElectricEquipDto eventDto)
        {
            if (ModelState.ErrorCount == 0)
            {
                await _equipService.Update(eventDto);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }

            if (Request.IsAjaxRequest())
                return PartialView("Edit", eventDto);
            else
                return View("Edit", eventDto);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            var model = await _equipService.Get(id);
            var result = _equipService.Delete(model);

            return new JsonResult(new { success = true, data = result });
        }
    }
}