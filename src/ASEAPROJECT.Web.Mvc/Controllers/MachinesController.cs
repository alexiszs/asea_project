﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Extensions;
using ASEAPROJECT.Controllers;
using ASEAPROJECT.Dtos.Machines;
using ASEAPROJECT.Services.MachineService;
using Microsoft.AspNetCore.Mvc;

namespace ASEAPROJECT.Web.Mvc.Controllers
{
    public class MachinesController : ASEAPROJECTControllerBase
    {
        private readonly IMachineService _machineService;

        public MachinesController(IMachineService machineService)
        {
            _machineService = machineService;
        }
        public async Task<IActionResult> Index()
        {
            var items = (await _machineService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            return View(items);
        }


        public IActionResult Create()
        {
            if (Request.IsAjaxRequest())
                return PartialView("Create");
            else
                return View("Create");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(MachineDto item)
        {
            if (ModelState.ErrorCount == 0)
            {
                await _machineService.Create(item);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }
            if (Request.IsAjaxRequest())
                return PartialView("Create", item);
            else
                return View("Create", item);
        }

        public async Task<IActionResult> Edit(int id)
        {
            var model = await _machineService.Get(id);
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(MachineDto item)
        {
            if (ModelState.ErrorCount == 0)
            {
                await _machineService.Update(item);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }

            if (Request.IsAjaxRequest())
                return PartialView("Edit", item);
            else
                return View("Edit", item);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            var model = await _machineService.Get(id);
            var result = _machineService.Delete(model);

            return new JsonResult(new { success = true, data = result });
        }
    }
}