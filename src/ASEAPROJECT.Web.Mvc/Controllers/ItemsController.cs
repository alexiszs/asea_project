﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ASEAPROJECT.Controllers;
using Abp.AspNetCore.Mvc.Extensions;
using ASEAPROJECT.Services.EquipItemService;
using ASEAPROJECT.Dtos.EquipItems;
using Abp.Application.Services.Dto;

namespace ASEAPROJECT.Web.Mvc.Controllers
{
    public class ItemsController : ASEAPROJECTControllerBase
    {
        private readonly IEquipItemService _itemService;

        public ItemsController(IEquipItemService itemService)
        {
            _itemService = itemService;
        }

        public async Task<IActionResult> Index()
        {
            var items = (await _itemService.GetAll(new PagedAndSortedResultRequestDto())).Items;
            return View(items);
        }

        public IActionResult Create()
        {
            if (Request.IsAjaxRequest())
                return PartialView("Create");
            else
                return View("Create");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(EquipItemDto equip)
        {
            if(ModelState.ErrorCount == 0)
            {
                await _itemService.Create(equip);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }
            if (Request.IsAjaxRequest())
                return PartialView("Create", equip);
            else
                return View("Create", equip);
        }

        public async Task<IActionResult> Edit(int id)
        {
            var model = await _itemService.Get(id);
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EquipItemDto equip)
        {
            if (ModelState.ErrorCount == 0)
            {
                await _itemService.Update(equip);
                if (Request.IsAjaxRequest())
                    return Content("OK");
                else
                    return RedirectToAction("Index");
            }

            if (Request.IsAjaxRequest())
                return PartialView("Edit", equip);
            else
                return View("Edit", equip);
        }

        // POST: Event/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            var model = await _itemService.Get(id);
            var result = _itemService.Delete(model);

            return new JsonResult(new { success = true, data = result });
        }
    }
}