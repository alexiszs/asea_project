﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ASEAPROJECT.Configuration;

namespace ASEAPROJECT.Web.Startup
{
    [DependsOn(typeof(ASEAPROJECTWebCoreModule))]
    public class ASEAPROJECTWebMvcModule : AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public ASEAPROJECTWebMvcModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void PreInitialize()
        {
            Configuration.Navigation.Providers.Add<ASEAPROJECTNavigationProvider>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ASEAPROJECTWebMvcModule).GetAssembly());
        }
    }
}
