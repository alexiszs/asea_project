﻿namespace ASEAPROJECT.Web.Startup
{
    public class PageNames
    {
        public const string Home = "Home";
        public const string About = "About";
        public const string Tenants = "Tenants";
        public const string Users = "Users";
        public const string Roles = "Roles";
        public const string Catalogs = "Catalogs";
        public const string Events = "Events";
        public const string Frecuencies = "Frecuencies";
        public const string Items = "Items";
        public const string ExternalRoles = "ExternalRoles";
        public const string TemporaryStorages = "TemporaryStorages";
        public const string Firms = "Firms";
        public const string Logs = "Logs";
        public const string ActivityLogs = "ActivityLogs";
        public const string ResidualLogs = "ResidualLogs";
        public const string CommonItems = "Common Items";
        public const string DangerResidues = "Danger Residues";
        public const string DispatcItems = "Dispatch Items";
        public const string DispenserItems = "Dispenser Items";
        public const string ElectricEquip = "Electric Equips";
        public const string ExtinguisherItems = "Extinguisher Controllers";
        public const string GroceryItems = "Grocery Items";
        public const string MensBathroom = "Mens Bathroom";
        public const string Tanks = "Tanks Items";
        public const string WomensBathroom = "Womens Bathroom";
        public const string DailyVerficationLogs = "Daily Verification Logs";
        public const string Machines = "Machines";

    }
}
