﻿using Abp.Application.Navigation;
using Abp.Localization;
using ASEAPROJECT.Authorization;

namespace ASEAPROJECT.Web.Startup
{
    /// <summary>
    /// This class defines menus for the application.
    /// </summary>
    public class ASEAPROJECTNavigationProvider : NavigationProvider
    {
        public override void SetNavigation(INavigationProviderContext context)
        {
            context.Manager.MainMenu
                .AddItem(
                    new MenuItemDefinition(
                        PageNames.Home,
                        L("HomePage"),
                        url: "",
                        icon: "home",
                        requiresAuthentication: true
                    )
                ).AddItem(
                    new MenuItemDefinition(
                        PageNames.Tenants,
                        L("Tenants"),
                        url: "Tenants",
                        icon: "business",
                        requiredPermissionName: PermissionNames.Pages_Tenants
                    )
                ).AddItem(
                    new MenuItemDefinition(
                        PageNames.Users,
                        L("Users"),
                        url: "Users",
                        icon: "people",
                        requiredPermissionName: PermissionNames.Pages_Users
                    )
                ).AddItem(
                    new MenuItemDefinition(
                        PageNames.Roles,
                        L("Roles"),
                        url: "Roles",
                        icon: "local_offer",
                        requiredPermissionName: PermissionNames.Pages_Roles
                    )
                )
                .AddItem(
                new MenuItemDefinition(
                    PageNames.Catalogs,
                    L("Catalogs"),
                    icon: ""
                    ).AddItem(
                    new MenuItemDefinition(
                        PageNames.Events,
                        L("Events"),
                        url: "Events",
                        icon:""
                        )
                    ).AddItem(
                    new MenuItemDefinition(
                        PageNames.Frecuencies,
                        L("Frecuencies"),
                        url: "Frecuencies",
                        icon:""
                        )
                    ).AddItem(
                    new MenuItemDefinition(
                        PageNames.Items,
                        L("Items"),
                        url: "Items",
                        icon: ""
                        )
                    ).AddItem(
                    new MenuItemDefinition(
                        PageNames.ExternalRoles,
                        L("ExternalRoles"),
                        url: "ExternalRoles",
                        icon: ""
                        )
                    ).AddItem(
                    new MenuItemDefinition(
                        PageNames.TemporaryStorages,
                        L("TemporaryStorages"),
                        url:"TemporaryStorage",
                        icon:""
                        )
                    ).AddItem(
                    new MenuItemDefinition(
                        PageNames.CommonItems,
                        L("CommonItems"),
                        url: "CommonItems",
                        icon: ""
                        )
                    ).AddItem(
                    new MenuItemDefinition(
                        PageNames.DangerResidues,
                        L("DangerResidues"),
                        url:"DangerResidues",
                        icon:""
                        )
                    ).AddItem(
                    new MenuItemDefinition(
                        PageNames.DispatcItems,
                        L("DispatchItems"),
                        url: "DispatchItems",
                        icon: ""
                        )
                    ).AddItem(
                    new MenuItemDefinition(
                        PageNames.DispenserItems,
                        L("DispenserItems"),
                        url: "DispenserItems",
                        icon: ""
                        )
                    ).AddItem(
                    new MenuItemDefinition(
                        PageNames.ElectricEquip,
                        L("ElectricEquip"),
                        url: "ElectricEquip",
                        icon: ""
                        )
                    ).AddItem(
                    new MenuItemDefinition(
                        PageNames.ExtinguisherItems,
                        L("ExtinguisherItems"),
                        url: "ExtinguisherItems",
                        icon: ""
                        )
                    ).AddItem(
                    new MenuItemDefinition(
                        PageNames.GroceryItems,
                        L("GroceryItems"),
                        url: "GroceryItems",
                        icon: ""
                        )
                    ).AddItem(
                    new MenuItemDefinition(
                        PageNames.MensBathroom,
                        L("MensBathroom"),
                        url: "MensBathroom",
                        icon: ""
                        )
                    ).AddItem(
                    new MenuItemDefinition(
                        PageNames.Machines,
                        L("Machines"),
                        url: "Machines",
                        icon: ""
                        )
                    ).AddItem(
                    new MenuItemDefinition(
                        PageNames.Tanks,
                        L("Tanks"),
                        url: "Tanks",
                        icon: ""
                        )
                    ).AddItem(
                    new MenuItemDefinition(
                        PageNames.WomensBathroom,
                        L("WomensBathroom"),
                        url: "WomensBathroom",
                        icon: ""
                        )
                    )
                ).AddItem(
                new MenuItemDefinition(
                    PageNames.Firms,
                    L("Firms"),
                    url: "Firms",
                    icon:""
                    )
                ).AddItem(
                new MenuItemDefinition(
                    PageNames.Logs,
                    L("Logs"),
                    url: "",
                    icon:""
                    ).AddItem(
                    new MenuItemDefinition(
                        PageNames.ActivityLogs,
                        L("ActivityLogs"),
                        url:"Logs",
                        icon:""
                        )
                    ).AddItem(
                new MenuItemDefinition(
                    PageNames.ResidualLogs,
                    L("ResidualLogs"),
                    url: "ResidualLogs",
                    icon: ""
                    )
                   ).AddItem(
                    new MenuItemDefinition(
                        PageNames.DailyVerficationLogs,
                        L("DailyVerificationLogs"),
                        url: "DailyLogs",
                        icon:""
                        )
                    )
                 );
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, ASEAPROJECTConsts.LocalizationSourceName);
        }
    }
}
