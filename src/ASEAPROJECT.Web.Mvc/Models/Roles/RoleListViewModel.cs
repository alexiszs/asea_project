﻿using System.Collections.Generic;
using ASEAPROJECT.Roles.Dto;

namespace ASEAPROJECT.Web.Models.Roles
{
    public class RoleListViewModel
    {
        public IReadOnlyList<RoleDto> Roles { get; set; }

        public IReadOnlyList<PermissionDto> Permissions { get; set; }
    }
}
