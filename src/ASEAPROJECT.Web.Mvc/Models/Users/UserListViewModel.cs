using System.Collections.Generic;
using ASEAPROJECT.Roles.Dto;
using ASEAPROJECT.Users.Dto;

namespace ASEAPROJECT.Web.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<UserDto> Users { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }
    }
}
