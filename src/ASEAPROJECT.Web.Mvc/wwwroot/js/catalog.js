﻿$(document).ready(function () {
    var dialog = $("#dialog").on('show.bs.modal', function (e) {
        var button = $(e.relatedTarget);

        $.ajax({
            async: true,
            url: button.data('action'),
            type: 'GET',
            data: { id: button.data('id') },
        }).done(function (response) {
            $('div.modal-body').html(response);
        }).fail(function (response) {
            alert("There was an error processing your request");
        });
    });

    $('div#dialog').on('click', '#save', function (e) {
        var form = $('#paramForm');
        var fd = new FormData(form[0]);

        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: fd,
            processData: false,
            contentType: false
        }).done(function (response) {
            if (response != 'OK') {
                $('div.modal-body').html(response);
            } else {
                dialog.modal('hide');
                location.reload();
            }
        }).fail(function (response) {
            alert("There was an error processing your request");
        });

    });

    $('#delete').on('click', function (e) {
        var $this = this;
        var URL = $this.getAttribute("url");
        var ID = $this.getAttribute("data-Dto-id");

        $.ajax({
            url: URL,
            type: 'POST',
            data: {
                id: ID
            }
        }).done(function (response) {
            console.log(response.result)
            if (response.result.success) {
                location.reload();
            } else {
                console.log(response.result.data);
            }
        });

    });
});