using Microsoft.Extensions.Configuration;
using Castle.MicroKernel.Registration;
using Abp.Events.Bus;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ASEAPROJECT.Configuration;
using ASEAPROJECT.EntityFrameworkCore;
using ASEAPROJECT.Migrator.DependencyInjection;

namespace ASEAPROJECT.Migrator
{
    [DependsOn(typeof(ASEAPROJECTEntityFrameworkModule))]
    public class ASEAPROJECTMigratorModule : AbpModule
    {
        private readonly IConfigurationRoot _appConfiguration;

        public ASEAPROJECTMigratorModule(ASEAPROJECTEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbSeed = true;

            _appConfiguration = AppConfigurations.Get(
                typeof(ASEAPROJECTMigratorModule).GetAssembly().GetDirectoryPathOrNull()
            );
        }

        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(
                ASEAPROJECTConsts.ConnectionStringName
            );

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
            Configuration.ReplaceService(
                typeof(IEventBus), 
                () => IocManager.IocContainer.Register(
                    Component.For<IEventBus>().Instance(NullEventBus.Instance)
                )
            );
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ASEAPROJECTMigratorModule).GetAssembly());
            ServiceCollectionRegistrar.Register(IocManager);
        }
    }
}
