using System.ComponentModel.DataAnnotations;

namespace ASEAPROJECT.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}