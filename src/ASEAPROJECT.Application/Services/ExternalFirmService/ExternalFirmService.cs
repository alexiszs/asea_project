﻿using Abp.Application.Services;
using ASEAPROJECT.Dtos.ExternalFirms;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Abp.Domain.Repositories;
using Abp.Application.Services.Dto;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using ASEAPROJECT.Services.RolesFirmService;
using ASEAPROJECT.Dtos.RolesFirm;
using AutoMapper;

namespace ASEAPROJECT.Services.ExternalFirmService
{
   public class ExternalFirmService : AsyncCrudAppService<ExternalFirm, ExternalFirmDto>, IExternalFirmService
    {
        private readonly IRolesFirmService _rolesFirmService;
        public ExternalFirmService(IRepository<ExternalFirm, int> repository, IRolesFirmService rolesFirmsService ) : base(repository)
        {
            _rolesFirmService = rolesFirmsService;
        }

        protected override string GetPermissionName { get => base.GetPermissionName; set => base.GetPermissionName = value; }
        protected override string GetAllPermissionName { get => base.GetAllPermissionName; set => base.GetAllPermissionName = value; }
        protected override string CreatePermissionName { get => base.CreatePermissionName; set => base.CreatePermissionName = value; }
        protected override string UpdatePermissionName { get => base.UpdatePermissionName; set => base.UpdatePermissionName = value; }
        protected override string DeletePermissionName { get => base.DeletePermissionName; set => base.DeletePermissionName = value; }

        public override Task<ExternalFirmDto> Create(ExternalFirmDto input)
        {
            return base.Create(input);
        }

        public override Task Delete(EntityDto<int> input)
        {
            return base.Delete(input);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public async Task<ExternalFirmDto> Get(int id)
        {
            var eventlist = await base.GetAll(new PagedAndSortedResultRequestDto());
            var modelList = new List<ExternalFirmDto>();
            modelList = (List<ExternalFirmDto>)eventlist.Items;

            return modelList.FirstOrDefault(element => element.Id == id);
        }

        public override Task<PagedResultDto<ExternalFirmDto>> GetAll(PagedAndSortedResultRequestDto input)
        {
            return base.GetAll(input);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override Task<ExternalFirmDto> Update(ExternalFirmDto input)
        {
            var list = _rolesFirmService.GetAllByFirm(input.Id);
            var roles = new List<RolesFirmsDto>();
            roles = list;
            
            foreach(var item in roles)
            {
                _rolesFirmService.Delete(item);
            }


            return base.Update(input);
        }

        public async Task<List<ExternalFirmDto>> GetAllwithroles()
        {
            var firms = await GetAll(new PagedAndSortedResultRequestDto());
            var firmswithroles = new List<ExternalFirmDto>();

            foreach(ExternalFirmDto firm in firms.Items)
            {
                firm.roles = Mapper.Map<List<RolesFirms>>(_rolesFirmService.GetAllByFirm(firm.Id));
                firmswithroles.Add(firm);
            }

            return firmswithroles;
        }

        protected override IQueryable<ExternalFirm> ApplyPaging(IQueryable<ExternalFirm> query, PagedAndSortedResultRequestDto input)
        {
            return base.ApplyPaging(query, input);
        }

        protected override IQueryable<ExternalFirm> ApplySorting(IQueryable<ExternalFirm> query, PagedAndSortedResultRequestDto input)
        {
            return base.ApplySorting(query, input);
        }

        protected override void CheckCreatePermission()
        {
            base.CheckCreatePermission();
        }

        protected override void CheckDeletePermission()
        {
            base.CheckDeletePermission();
        }

        protected override void CheckGetAllPermission()
        {
            base.CheckGetAllPermission();
        }

        protected override void CheckGetPermission()
        {
            base.CheckGetPermission();
        }

        protected override void CheckPermission(string permissionName)
        {
            base.CheckPermission(permissionName);
        }

        protected override void CheckUpdatePermission()
        {
            base.CheckUpdatePermission();
        }

        protected override IQueryable<ExternalFirm> CreateFilteredQuery(PagedAndSortedResultRequestDto input)
        {
            return base.CreateFilteredQuery(input);
        }

        protected override Task<ExternalFirm> GetEntityByIdAsync(int id)
        {
            return base.GetEntityByIdAsync(id);
        }

        protected override bool IsEnabled(string featureName)
        {
            return base.IsEnabled(featureName);
        }

        protected override Task<bool> IsEnabledAsync(string featureName)
        {
            return base.IsEnabledAsync(featureName);
        }

        protected override bool IsGranted(string permissionName)
        {
            return base.IsGranted(permissionName);
        }

        protected override Task<bool> IsGrantedAsync(string permissionName)
        {
            return base.IsGrantedAsync(permissionName);
        }

        protected override string L(string name)
        {
            return base.L(name);
        }

        protected override string L(string name, CultureInfo culture)
        {
            return base.L(name, culture);
        }

        protected override ExternalFirm MapToEntity(ExternalFirmDto createInput)
        {
            return base.MapToEntity(createInput);
        }

        protected override void MapToEntity(ExternalFirmDto updateInput, ExternalFirm entity)
        {
            base.MapToEntity(updateInput, entity);
        }

        protected override ExternalFirmDto MapToEntityDto(ExternalFirm entity)
        {
            return base.MapToEntityDto(entity);
        }
    }
}
