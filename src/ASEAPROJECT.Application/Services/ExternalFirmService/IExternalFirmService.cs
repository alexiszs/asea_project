﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.ExternalFirms;

namespace ASEAPROJECT.Services.ExternalFirmService
{
    public interface IExternalFirmService
    {
        Task<ExternalFirmDto> Create(ExternalFirmDto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<ExternalFirmDto> Get(int input);
        Task<PagedResultDto<ExternalFirmDto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<ExternalFirmDto> Update(ExternalFirmDto input);
        Task<List<ExternalFirmDto>> GetAllwithroles();
    }
}