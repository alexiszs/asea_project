﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.MensBathrooms;

namespace ASEAPROJECT.Services.MensBathroomService
{
    public interface IMensBathroomService
    {
        Task<MensBathroomDto> Create(MensBathroomDto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<MensBathroomDto> Get(int id);
        Task<PagedResultDto<MensBathroomDto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<MensBathroomDto> Update(MensBathroomDto input);
    }
}