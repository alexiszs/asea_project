﻿using Abp.Application.Services;
using ASEAPROJECT.Dtos.Events;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Abp.Domain.Repositories;
using Abp.Application.Services.Dto;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace ASEAPROJECT.Services.EventService
{
    public class EventService : AsyncCrudAppService<Event, EventDto>, IEventService
    {

        public EventService(IRepository<Event, int> repository) : base(repository)
        {
        }

        public override Task<EventDto> Create(EventDto input)
        {
            return base.Create(input);
        }

        public override Task Delete(EntityDto<int> input)
        {
            return base.Delete(input);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public async Task<EventDto> Get(int id)
        {
            var eventlist = await base.GetAll(new PagedAndSortedResultRequestDto());
            var modelList = new List<EventDto>();
            modelList = (List<EventDto>)eventlist.Items;

            return  modelList.FirstOrDefault(element => element.Id == id);
        }

        public override Task<PagedResultDto<EventDto>> GetAll(PagedAndSortedResultRequestDto input)
        {
            return base.GetAll(input);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override Task<EventDto> Update(EventDto input)
        {
            return base.Update(input);
        }

        protected override IQueryable<Event> ApplyPaging(IQueryable<Event> query, PagedAndSortedResultRequestDto input)
        {
            return base.ApplyPaging(query, input);
        }

        protected override IQueryable<Event> ApplySorting(IQueryable<Event> query, PagedAndSortedResultRequestDto input)
        {
            return base.ApplySorting(query, input);
        }

        protected override void CheckCreatePermission()
        {
            base.CheckCreatePermission();
        }

        protected override void CheckDeletePermission()
        {
            base.CheckDeletePermission();
        }

        protected override void CheckGetAllPermission()
        {
            base.CheckGetAllPermission();
        }

        protected override void CheckGetPermission()
        {
            base.CheckGetPermission();
        }

        protected override void CheckPermission(string permissionName)
        {
            base.CheckPermission(permissionName);
        }

        protected override void CheckUpdatePermission()
        {
            base.CheckUpdatePermission();
        }

        protected override IQueryable<Event> CreateFilteredQuery(PagedAndSortedResultRequestDto input)
        {
            return base.CreateFilteredQuery(input);
        }

        protected override Task<Event> GetEntityByIdAsync(int id)
        {
            return base.GetEntityByIdAsync(id);
        }

        protected override bool IsEnabled(string featureName)
        {
            return base.IsEnabled(featureName);
        }

        protected override Task<bool> IsEnabledAsync(string featureName)
        {
            return base.IsEnabledAsync(featureName);
        }

        protected override bool IsGranted(string permissionName)
        {
            return base.IsGranted(permissionName);
        }

        protected override Task<bool> IsGrantedAsync(string permissionName)
        {
            return base.IsGrantedAsync(permissionName);
        }

        protected override string L(string name)
        {
            return base.L(name);
        }

        protected override string L(string name, CultureInfo culture)
        {
            return base.L(name, culture);
        }

        protected override Event MapToEntity(EventDto createInput)
        {
            return base.MapToEntity(createInput);
        }

        protected override void MapToEntity(EventDto updateInput, Event entity)
        {
            base.MapToEntity(updateInput, entity);
        }

        protected override EventDto MapToEntityDto(Event entity)
        {
            return base.MapToEntityDto(entity);
        }
    }
}
