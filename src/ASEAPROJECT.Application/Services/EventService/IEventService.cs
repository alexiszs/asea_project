﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.Events;

namespace ASEAPROJECT.Services.EventService
{
    public interface IEventService
    {
        Task<EventDto> Create(EventDto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<EventDto> Get(int id);
        Task<PagedResultDto<EventDto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<EventDto> Update(EventDto input);
    }
}