﻿using Abp.Application.Services;
using ASEAPROJECT.Dtos.RolesFirm;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Abp.Domain.Repositories;
using Abp.Application.Services.Dto;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace ASEAPROJECT.Services.RolesFirmService
{
    public class RolesFirmService : AsyncCrudAppService<RolesFirms, RolesFirmsDto>, IRolesFirmService
    {
        public RolesFirmService(IRepository<RolesFirms, int> repository) : base(repository)
        {
        }

        protected override string GetPermissionName { get => base.GetPermissionName; set => base.GetPermissionName = value; }
        protected override string GetAllPermissionName { get => base.GetAllPermissionName; set => base.GetAllPermissionName = value; }
        protected override string CreatePermissionName { get => base.CreatePermissionName; set => base.CreatePermissionName = value; }
        protected override string UpdatePermissionName { get => base.UpdatePermissionName; set => base.UpdatePermissionName = value; }
        protected override string DeletePermissionName { get => base.DeletePermissionName; set => base.DeletePermissionName = value; }

        public override Task<RolesFirmsDto> Create(RolesFirmsDto input)
        {
            return base.Create(input);
        }

        public override Task Delete(EntityDto<int> input)
        {
            return base.Delete(input);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public async Task<RolesFirmsDto> Get(int id)
        {
            var rolesfirms = await GetAll(new PagedAndSortedResultRequestDto());
            var list = new List<RolesFirmsDto>();
            list = (List<RolesFirmsDto>)rolesfirms.Items;
            return list.FirstOrDefault(r => r.Id == id);
        }

        public override Task<PagedResultDto<RolesFirmsDto>> GetAll(PagedAndSortedResultRequestDto input)
        {
            return base.GetAll(input);
        }

        public List<RolesFirmsDto> GetAllByFirm(int id)
        {
            var roles = GetAll(new PagedAndSortedResultRequestDto()).Result;
            var list = new List<RolesFirmsDto>();
            list = (List<RolesFirmsDto>)roles.Items;
            return list.Where(r => r.firm_id == id).ToList();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override Task<RolesFirmsDto> Update(RolesFirmsDto input)
        {
            return base.Update(input);
        }

        protected override IQueryable<RolesFirms> ApplyPaging(IQueryable<RolesFirms> query, PagedAndSortedResultRequestDto input)
        {
            return base.ApplyPaging(query, input);
        }

        protected override IQueryable<RolesFirms> ApplySorting(IQueryable<RolesFirms> query, PagedAndSortedResultRequestDto input)
        {
            return base.ApplySorting(query, input);
        }

        protected override void CheckCreatePermission()
        {
            base.CheckCreatePermission();
        }

        protected override void CheckDeletePermission()
        {
            base.CheckDeletePermission();
        }

        protected override void CheckGetAllPermission()
        {
            base.CheckGetAllPermission();
        }

        protected override void CheckGetPermission()
        {
            base.CheckGetPermission();
        }

        protected override void CheckPermission(string permissionName)
        {
            base.CheckPermission(permissionName);
        }

        protected override void CheckUpdatePermission()
        {
            base.CheckUpdatePermission();
        }

        protected override IQueryable<RolesFirms> CreateFilteredQuery(PagedAndSortedResultRequestDto input)
        {
            return base.CreateFilteredQuery(input);
        }

        protected override Task<RolesFirms> GetEntityByIdAsync(int id)
        {
            return base.GetEntityByIdAsync(id);
        }

        protected override bool IsEnabled(string featureName)
        {
            return base.IsEnabled(featureName);
        }

        protected override Task<bool> IsEnabledAsync(string featureName)
        {
            return base.IsEnabledAsync(featureName);
        }

        protected override bool IsGranted(string permissionName)
        {
            return base.IsGranted(permissionName);
        }

        protected override Task<bool> IsGrantedAsync(string permissionName)
        {
            return base.IsGrantedAsync(permissionName);
        }

        protected override string L(string name)
        {
            return base.L(name);
        }

        protected override string L(string name, CultureInfo culture)
        {
            return base.L(name, culture);
        }

        protected override RolesFirms MapToEntity(RolesFirmsDto createInput)
        {
            return base.MapToEntity(createInput);
        }

        protected override void MapToEntity(RolesFirmsDto updateInput, RolesFirms entity)
        {
            base.MapToEntity(updateInput, entity);
        }

        protected override RolesFirmsDto MapToEntityDto(RolesFirms entity)
        {
            return base.MapToEntityDto(entity);
        }
    }
}
