﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.RolesFirm;
using System.Collections.Generic;

namespace ASEAPROJECT.Services.RolesFirmService
{
    public interface IRolesFirmService
    {
        Task<RolesFirmsDto> Create(RolesFirmsDto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<RolesFirmsDto> Get(int id);
        Task<PagedResultDto<RolesFirmsDto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<RolesFirmsDto> Update(RolesFirmsDto input);
        List<RolesFirmsDto> GetAllByFirm(int id);

    }
}