﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.Mens_Bathroom_In_Logs;

namespace ASEAPROJECT.Services.Mens_bathroom_In_LogService
{
    public interface IMens_bathroom_In_LogService
    {
        Task<Mens_bathroom_In_Log_Dto> Create(Mens_bathroom_In_Log_Dto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<Mens_bathroom_In_Log_Dto> Get(EntityDto<int> input);
        Task<PagedResultDto<Mens_bathroom_In_Log_Dto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<Mens_bathroom_In_Log_Dto> Update(Mens_bathroom_In_Log_Dto input);
        Task<List<Mens_bathroom_In_Log_Dto>> GetAllByLog(int id);
    }
}