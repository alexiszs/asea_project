﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ASEAPROJECT.Dtos.Tanks_in_Daily_logs;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEAPROJECT.Services.Tanks_in_Daily_LogsService
{
    public class Tanks_in_Daily_LogsService : AsyncCrudAppService<Tanks_in_Daily_Logs, Tanks_in_Daily_Logs_Dto>, ITanks_in_Daily_LogsService
    {
        public Tanks_in_Daily_LogsService(IRepository<Tanks_in_Daily_Logs, int> repository) : base(repository)
        {
        }

        protected override string GetPermissionName { get => base.GetPermissionName; set => base.GetPermissionName = value; }
        protected override string GetAllPermissionName { get => base.GetAllPermissionName; set => base.GetAllPermissionName = value; }
        protected override string CreatePermissionName { get => base.CreatePermissionName; set => base.CreatePermissionName = value; }
        protected override string UpdatePermissionName { get => base.UpdatePermissionName; set => base.UpdatePermissionName = value; }
        protected override string DeletePermissionName { get => base.DeletePermissionName; set => base.DeletePermissionName = value; }

        public override Task<Tanks_in_Daily_Logs_Dto> Create(Tanks_in_Daily_Logs_Dto input)
        {
            return base.Create(input);
        }

        public override Task Delete(EntityDto<int> input)
        {
            return base.Delete(input);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override Task<Tanks_in_Daily_Logs_Dto> Get(EntityDto<int> input)
        {
            return base.Get(input);
        }

        public override Task<PagedResultDto<Tanks_in_Daily_Logs_Dto>> GetAll(PagedAndSortedResultRequestDto input)
        {
            return base.GetAll(input);
        }

        public async Task<List<Tanks_in_Daily_Logs_Dto>> GetAllByLog(int id)
        {
            var listofitems = await GetAll(new PagedAndSortedResultRequestDto());
            var list = new List<Tanks_in_Daily_Logs_Dto>();
            list = listofitems.Items.ToList().Where(i => i.log_id == id).ToList();
            return list;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override Task<Tanks_in_Daily_Logs_Dto> Update(Tanks_in_Daily_Logs_Dto input)
        {
            return base.Update(input);
        }

        protected override IQueryable<Tanks_in_Daily_Logs> ApplyPaging(IQueryable<Tanks_in_Daily_Logs> query, PagedAndSortedResultRequestDto input)
        {
            return base.ApplyPaging(query, input);
        }

        protected override IQueryable<Tanks_in_Daily_Logs> ApplySorting(IQueryable<Tanks_in_Daily_Logs> query, PagedAndSortedResultRequestDto input)
        {
            return base.ApplySorting(query, input);
        }

        protected override void CheckCreatePermission()
        {
            base.CheckCreatePermission();
        }

        protected override void CheckDeletePermission()
        {
            base.CheckDeletePermission();
        }

        protected override void CheckGetAllPermission()
        {
            base.CheckGetAllPermission();
        }

        protected override void CheckGetPermission()
        {
            base.CheckGetPermission();
        }

        protected override void CheckPermission(string permissionName)
        {
            base.CheckPermission(permissionName);
        }

        protected override void CheckUpdatePermission()
        {
            base.CheckUpdatePermission();
        }

        protected override IQueryable<Tanks_in_Daily_Logs> CreateFilteredQuery(PagedAndSortedResultRequestDto input)
        {
            return base.CreateFilteredQuery(input);
        }

        protected override Task<Tanks_in_Daily_Logs> GetEntityByIdAsync(int id)
        {
            return base.GetEntityByIdAsync(id);
        }

        protected override bool IsEnabled(string featureName)
        {
            return base.IsEnabled(featureName);
        }

        protected override Task<bool> IsEnabledAsync(string featureName)
        {
            return base.IsEnabledAsync(featureName);
        }

        protected override bool IsGranted(string permissionName)
        {
            return base.IsGranted(permissionName);
        }

        protected override Task<bool> IsGrantedAsync(string permissionName)
        {
            return base.IsGrantedAsync(permissionName);
        }

        protected override string L(string name)
        {
            return base.L(name);
        }

        protected override string L(string name, CultureInfo culture)
        {
            return base.L(name, culture);
        }

        protected override Tanks_in_Daily_Logs MapToEntity(Tanks_in_Daily_Logs_Dto createInput)
        {
            return base.MapToEntity(createInput);
        }

        protected override void MapToEntity(Tanks_in_Daily_Logs_Dto updateInput, Tanks_in_Daily_Logs entity)
        {
            base.MapToEntity(updateInput, entity);
        }

        protected override Tanks_in_Daily_Logs_Dto MapToEntityDto(Tanks_in_Daily_Logs entity)
        {
            return base.MapToEntityDto(entity);
        }
    }
}
