﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.Tanks_in_Daily_logs;

namespace ASEAPROJECT.Services.Tanks_in_Daily_LogsService
{
    public interface ITanks_in_Daily_LogsService
    {
        Task<Tanks_in_Daily_Logs_Dto> Create(Tanks_in_Daily_Logs_Dto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<Tanks_in_Daily_Logs_Dto> Get(EntityDto<int> input);
        Task<PagedResultDto<Tanks_in_Daily_Logs_Dto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<Tanks_in_Daily_Logs_Dto> Update(Tanks_in_Daily_Logs_Dto input);
        Task<List<Tanks_in_Daily_Logs_Dto>> GetAllByLog(int id);
    }
}