﻿using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.ResidualLogs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ASEAPROJECT.Services.ResidualLogService
{
   public interface IResidualLogService
    {
        Task<ResidualLogDto> Create(ResidualLogDto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<ResidualLogDto> Get(int id);
        Task<PagedResultDto<ResidualLogDto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<ResidualLogDto> Update(ResidualLogDto input);
    }
}
