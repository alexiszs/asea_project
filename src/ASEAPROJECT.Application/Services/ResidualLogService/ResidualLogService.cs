﻿using Abp.Application.Services;
using ASEAPROJECT.Dtos.ResidualLogs;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using ASEAPROJECT.Services.EquipLogService;
using ASEAPROJECT.Dtos.EquipLogs;

namespace ASEAPROJECT.Services.ResidualLogService
{
    public class ResidualLogService : AsyncCrudAppService<ResidualLog, ResidualLogDto>, IResidualLogService
    {
        private readonly IEquipLogService _equipLogService;
        public ResidualLogService(IRepository<ResidualLog, int> repository, IEquipLogService equipLogService) : base(repository)
        {
            _equipLogService = equipLogService;
        }

        protected override string GetPermissionName { get => base.GetPermissionName; set => base.GetPermissionName = value; }
        protected override string GetAllPermissionName { get => base.GetAllPermissionName; set => base.GetAllPermissionName = value; }
        protected override string CreatePermissionName { get => base.CreatePermissionName; set => base.CreatePermissionName = value; }
        protected override string UpdatePermissionName { get => base.UpdatePermissionName; set => base.UpdatePermissionName = value; }
        protected override string DeletePermissionName { get => base.DeletePermissionName; set => base.DeletePermissionName = value; }

        public override Task<ResidualLogDto> Create(ResidualLogDto input)
        {
            return base.Create(input);
        }

        public override Task Delete(EntityDto<int> input)
        {
            return base.Delete(input);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public async Task<ResidualLogDto> Get(int id)
        {
            var eventlist = await base.GetAll(new PagedAndSortedResultRequestDto());
            var modelList = new List<ResidualLogDto>();
            modelList = (List<ResidualLogDto>)eventlist.Items;

            return modelList.FirstOrDefault(element => element.Id == id);
        }

        public override Task<PagedResultDto<ResidualLogDto>> GetAll(PagedAndSortedResultRequestDto input)
        {
            return base.GetAll(input);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override Task<ResidualLogDto> Update(ResidualLogDto input)
        {
            var list = _equipLogService.GetAllByLog(input.Id);
            var items = new List<EquipLogDto>();
            foreach(var item in items)
            {
                _equipLogService.Delete(item);
            }
            return base.Update(input);
        }

        protected override IQueryable<ResidualLog> ApplyPaging(IQueryable<ResidualLog> query, PagedAndSortedResultRequestDto input)
        {
            return base.ApplyPaging(query, input);
        }

        protected override IQueryable<ResidualLog> ApplySorting(IQueryable<ResidualLog> query, PagedAndSortedResultRequestDto input)
        {
            return base.ApplySorting(query, input);
        }

        protected override void CheckCreatePermission()
        {
            base.CheckCreatePermission();
        }

        protected override void CheckDeletePermission()
        {
            base.CheckDeletePermission();
        }

        protected override void CheckGetAllPermission()
        {
            base.CheckGetAllPermission();
        }

        protected override void CheckGetPermission()
        {
            base.CheckGetPermission();
        }

        protected override void CheckPermission(string permissionName)
        {
            base.CheckPermission(permissionName);
        }

        protected override void CheckUpdatePermission()
        {
            base.CheckUpdatePermission();
        }

        protected override IQueryable<ResidualLog> CreateFilteredQuery(PagedAndSortedResultRequestDto input)
        {
            return base.CreateFilteredQuery(input);
        }

        protected override Task<ResidualLog> GetEntityByIdAsync(int id)
        {
            return base.GetEntityByIdAsync(id);
        }

        protected override bool IsEnabled(string featureName)
        {
            return base.IsEnabled(featureName);
        }

        protected override Task<bool> IsEnabledAsync(string featureName)
        {
            return base.IsEnabledAsync(featureName);
        }

        protected override bool IsGranted(string permissionName)
        {
            return base.IsGranted(permissionName);
        }

        protected override Task<bool> IsGrantedAsync(string permissionName)
        {
            return base.IsGrantedAsync(permissionName);
        }

        protected override string L(string name)
        {
            return base.L(name);
        }

        protected override string L(string name, CultureInfo culture)
        {
            return base.L(name, culture);
        }

        protected override ResidualLog MapToEntity(ResidualLogDto createInput)
        {
            return base.MapToEntity(createInput);
        }

        protected override void MapToEntity(ResidualLogDto updateInput, ResidualLog entity)
        {
            base.MapToEntity(updateInput, entity);
        }

        protected override ResidualLogDto MapToEntityDto(ResidualLog entity)
        {
            return base.MapToEntityDto(entity);
        }
    }
}
