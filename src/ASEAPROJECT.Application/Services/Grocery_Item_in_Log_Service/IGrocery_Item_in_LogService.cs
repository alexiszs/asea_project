﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.Grocery_Item_in_Logs;

namespace ASEAPROJECT.Services.Grocery_Item_in_Log_Service
{
    public interface IGrocery_Item_in_LogService
    {
        Task<Grocery_Item_in_Log_Dto> Create(Grocery_Item_in_Log_Dto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<Grocery_Item_in_Log_Dto> Get(EntityDto<int> input);
        Task<PagedResultDto<Grocery_Item_in_Log_Dto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<Grocery_Item_in_Log_Dto> Update(Grocery_Item_in_Log_Dto input);
        Task<List<Grocery_Item_in_Log_Dto>> GetAllByLog(int id);
    }
}