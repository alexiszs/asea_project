﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.Tanks;

namespace ASEAPROJECT.Services.TankService
{
    public interface ITankService
    {
        Task<TankDto> Create(TankDto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<TankDto> Get(int id);
        Task<PagedResultDto<TankDto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<TankDto> Update(TankDto input);
    }
}