﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.Employees_item_in_logs;

namespace ASEAPROJECT.Services.Employees_Item_in_logsService
{
    public interface IEmployees_Item_in_logsService
    {
        Task<Employees_Item_in_logs_Dto> Create(Employees_Item_in_logs_Dto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<Employees_Item_in_logs_Dto> Get(EntityDto<int> input);
        Task<PagedResultDto<Employees_Item_in_logs_Dto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<Employees_Item_in_logs_Dto> Update(Employees_Item_in_logs_Dto input);
        Task<List<Employees_Item_in_logs_Dto>> GetAllByLog(int id);
    }
}