﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.EquipLogs;
using System.Collections.Generic;

namespace ASEAPROJECT.Services.EquipLogService
{
    public interface IEquipLogService
    {
        Task<EquipLogDto> Create(EquipLogDto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<EquipLogDto> Get(int id);
        Task<PagedResultDto<EquipLogDto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<EquipLogDto> Update(EquipLogDto input);
        List<EquipLogDto> GetAllByLog(int id);
    }
}