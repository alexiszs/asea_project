﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.Electric_Room_in_logs;

namespace ASEAPROJECT.Services.Electric_Room_in_LogService
{
    public interface IElectric_Room_in_LogService
    {
        Task<Electric_Room_in_Log_Dto> Create(Electric_Room_in_Log_Dto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<Electric_Room_in_Log_Dto> Get(EntityDto<int> input);
        Task<PagedResultDto<Electric_Room_in_Log_Dto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<Electric_Room_in_Log_Dto> Update(Electric_Room_in_Log_Dto input);
        Task<List<Electric_Room_in_Log_Dto>> GetAllByLog(int id);
    }
}