﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.WomensBathrooms;

namespace ASEAPROJECT.Services.WomensBathroomService
{
    public interface IWomensBathroomService
    {
        Task<WomensBathroomDto> Create(WomensBathroomDto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<WomensBathroomDto> Get(int id);
        Task<PagedResultDto<WomensBathroomDto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<WomensBathroomDto> Update(WomensBathroomDto input);
    }
}