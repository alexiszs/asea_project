﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.EmployeItems;

namespace ASEAPROJECT.Services.EmployeItemService
{
    public interface IEmployeItemService
    {
        Task<EmployeItemDto> Create(EmployeItemDto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<EmployeItemDto> Get(int id);
        Task<PagedResultDto<EmployeItemDto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<EmployeItemDto> Update(EmployeItemDto input);
    }
}