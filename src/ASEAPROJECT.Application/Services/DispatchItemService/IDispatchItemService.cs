﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.DispatchItems;

namespace ASEAPROJECT.Services.DispatchItemService
{
    public interface IDispatchItemService
    {
        Task<DispatchItemDto> Create(DispatchItemDto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<DispatchItemDto> Get(int id);
        Task<PagedResultDto<DispatchItemDto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<DispatchItemDto> Update(DispatchItemDto input);
    }
}