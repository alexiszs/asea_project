﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.Machinery_in_Daily_log;

namespace ASEAPROJECT.Services.Machinery_in_Daily_logsService
{
    public interface IMachinery_in_Daily_logsService
    {
        Task<Machinery_in_Daily_logs_Dto> Create(Machinery_in_Daily_logs_Dto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<Machinery_in_Daily_logs_Dto> Get(EntityDto<int> input);
        Task<PagedResultDto<Machinery_in_Daily_logs_Dto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<Machinery_in_Daily_logs_Dto> Update(Machinery_in_Daily_logs_Dto input);
        Task<List<Machinery_in_Daily_logs_Dto>> GetAllByLog(int id);
    }
}