﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ASEAPROJECT.Dtos.Machinery_in_Daily_log;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEAPROJECT.Services.Machinery_in_Daily_logsService
{
    public class Machinery_in_Daily_logsService : AsyncCrudAppService<Machinery_in_Daily_logs, Machinery_in_Daily_logs_Dto>, IMachinery_in_Daily_logsService
    {
        public Machinery_in_Daily_logsService(IRepository<Machinery_in_Daily_logs, int> repository) : base(repository)
        {
        }

        protected override string GetPermissionName { get => base.GetPermissionName; set => base.GetPermissionName = value; }
        protected override string GetAllPermissionName { get => base.GetAllPermissionName; set => base.GetAllPermissionName = value; }
        protected override string CreatePermissionName { get => base.CreatePermissionName; set => base.CreatePermissionName = value; }
        protected override string UpdatePermissionName { get => base.UpdatePermissionName; set => base.UpdatePermissionName = value; }
        protected override string DeletePermissionName { get => base.DeletePermissionName; set => base.DeletePermissionName = value; }

        public override Task<Machinery_in_Daily_logs_Dto> Create(Machinery_in_Daily_logs_Dto input)
        {
            return base.Create(input);
        }

        public override Task Delete(EntityDto<int> input)
        {
            return base.Delete(input);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override Task<Machinery_in_Daily_logs_Dto> Get(EntityDto<int> input)
        {
            return base.Get(input);
        }

        public override Task<PagedResultDto<Machinery_in_Daily_logs_Dto>> GetAll(PagedAndSortedResultRequestDto input)
        {
            return base.GetAll(input);
        }

        public async Task<List<Machinery_in_Daily_logs_Dto>> GetAllByLog(int id)
        {
            var listitems = await GetAll(new PagedAndSortedResultRequestDto());
            var list = new List<Machinery_in_Daily_logs_Dto>();
            list = listitems.Items.ToList().Where(i => i.log_id == id).ToList();
            return list;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override Task<Machinery_in_Daily_logs_Dto> Update(Machinery_in_Daily_logs_Dto input)
        {
            return base.Update(input);
        }

        protected override IQueryable<Machinery_in_Daily_logs> ApplyPaging(IQueryable<Machinery_in_Daily_logs> query, PagedAndSortedResultRequestDto input)
        {
            return base.ApplyPaging(query, input);
        }

        protected override IQueryable<Machinery_in_Daily_logs> ApplySorting(IQueryable<Machinery_in_Daily_logs> query, PagedAndSortedResultRequestDto input)
        {
            return base.ApplySorting(query, input);
        }

        protected override void CheckCreatePermission()
        {
            base.CheckCreatePermission();
        }

        protected override void CheckDeletePermission()
        {
            base.CheckDeletePermission();
        }

        protected override void CheckGetAllPermission()
        {
            base.CheckGetAllPermission();
        }

        protected override void CheckGetPermission()
        {
            base.CheckGetPermission();
        }

        protected override void CheckPermission(string permissionName)
        {
            base.CheckPermission(permissionName);
        }

        protected override void CheckUpdatePermission()
        {
            base.CheckUpdatePermission();
        }

        protected override IQueryable<Machinery_in_Daily_logs> CreateFilteredQuery(PagedAndSortedResultRequestDto input)
        {
            return base.CreateFilteredQuery(input);
        }

        protected override Task<Machinery_in_Daily_logs> GetEntityByIdAsync(int id)
        {
            return base.GetEntityByIdAsync(id);
        }

        protected override bool IsEnabled(string featureName)
        {
            return base.IsEnabled(featureName);
        }

        protected override Task<bool> IsEnabledAsync(string featureName)
        {
            return base.IsEnabledAsync(featureName);
        }

        protected override bool IsGranted(string permissionName)
        {
            return base.IsGranted(permissionName);
        }

        protected override Task<bool> IsGrantedAsync(string permissionName)
        {
            return base.IsGrantedAsync(permissionName);
        }

        protected override string L(string name)
        {
            return base.L(name);
        }

        protected override string L(string name, CultureInfo culture)
        {
            return base.L(name, culture);
        }

        protected override Machinery_in_Daily_logs MapToEntity(Machinery_in_Daily_logs_Dto createInput)
        {
            return base.MapToEntity(createInput);
        }

        protected override void MapToEntity(Machinery_in_Daily_logs_Dto updateInput, Machinery_in_Daily_logs entity)
        {
            base.MapToEntity(updateInput, entity);
        }

        protected override Machinery_in_Daily_logs_Dto MapToEntityDto(Machinery_in_Daily_logs entity)
        {
            return base.MapToEntityDto(entity);
        }
    }
}
