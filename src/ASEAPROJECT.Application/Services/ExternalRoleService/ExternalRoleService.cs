﻿using Abp.Application.Services;
using ASEAPROJECT.Dtos.ExternalRoles;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Abp.Domain.Repositories;
using Abp.Application.Services.Dto;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace ASEAPROJECT.Services.ExternalRoleService
{
    public class ExternalRoleService : AsyncCrudAppService<ExternalRol, ExternalRoleDto>, IExternalRoleService
    {
        public ExternalRoleService(IRepository<ExternalRol, int> repository) : base(repository)
        {
        }

        protected override string GetPermissionName { get => base.GetPermissionName; set => base.GetPermissionName = value; }
        protected override string GetAllPermissionName { get => base.GetAllPermissionName; set => base.GetAllPermissionName = value; }
        protected override string CreatePermissionName { get => base.CreatePermissionName; set => base.CreatePermissionName = value; }
        protected override string UpdatePermissionName { get => base.UpdatePermissionName; set => base.UpdatePermissionName = value; }
        protected override string DeletePermissionName { get => base.DeletePermissionName; set => base.DeletePermissionName = value; }

        public override Task<ExternalRoleDto> Create(ExternalRoleDto input)
        {
            return base.Create(input);
        }

        public override Task Delete(EntityDto<int> input)
        {
            return base.Delete(input);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public async Task<ExternalRoleDto> Get(int id)
        {
            var Externalroles = await base.GetAll(new PagedAndSortedResultRequestDto());
            var items = new List<ExternalRoleDto>();
            items = (List<ExternalRoleDto>)Externalroles.Items;
            return items.FirstOrDefault(r => r.Id == id);
        }

        public override Task<PagedResultDto<ExternalRoleDto>> GetAll(PagedAndSortedResultRequestDto input)
        {
            return base.GetAll(input);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override Task<ExternalRoleDto> Update(ExternalRoleDto input)
        {
            return base.Update(input);
        }

        protected override IQueryable<ExternalRol> ApplyPaging(IQueryable<ExternalRol> query, PagedAndSortedResultRequestDto input)
        {
            return base.ApplyPaging(query, input);
        }

        protected override IQueryable<ExternalRol> ApplySorting(IQueryable<ExternalRol> query, PagedAndSortedResultRequestDto input)
        {
            return base.ApplySorting(query, input);
        }

        protected override void CheckCreatePermission()
        {
            base.CheckCreatePermission();
        }

        protected override void CheckDeletePermission()
        {
            base.CheckDeletePermission();
        }

        protected override void CheckGetAllPermission()
        {
            base.CheckGetAllPermission();
        }

        protected override void CheckGetPermission()
        {
            base.CheckGetPermission();
        }

        protected override void CheckPermission(string permissionName)
        {
            base.CheckPermission(permissionName);
        }

        protected override void CheckUpdatePermission()
        {
            base.CheckUpdatePermission();
        }

        protected override IQueryable<ExternalRol> CreateFilteredQuery(PagedAndSortedResultRequestDto input)
        {
            return base.CreateFilteredQuery(input);
        }

        protected override Task<ExternalRol> GetEntityByIdAsync(int id)
        {
            return base.GetEntityByIdAsync(id);
        }

        protected override bool IsEnabled(string featureName)
        {
            return base.IsEnabled(featureName);
        }

        protected override Task<bool> IsEnabledAsync(string featureName)
        {
            return base.IsEnabledAsync(featureName);
        }

        protected override bool IsGranted(string permissionName)
        {
            return base.IsGranted(permissionName);
        }

        protected override Task<bool> IsGrantedAsync(string permissionName)
        {
            return base.IsGrantedAsync(permissionName);
        }

        protected override string L(string name)
        {
            return base.L(name);
        }

        protected override string L(string name, CultureInfo culture)
        {
            return base.L(name, culture);
        }

        protected override ExternalRol MapToEntity(ExternalRoleDto createInput)
        {
            return base.MapToEntity(createInput);
        }

        protected override void MapToEntity(ExternalRoleDto updateInput, ExternalRol entity)
        {
            base.MapToEntity(updateInput, entity);
        }

        protected override ExternalRoleDto MapToEntityDto(ExternalRol entity)
        {
            return base.MapToEntityDto(entity);
        }
    }
}
