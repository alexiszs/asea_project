﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.ExternalRoles;
using System.Collections.Generic;

namespace ASEAPROJECT.Services.ExternalRoleService
{
    public interface IExternalRoleService
    {
        Task<ExternalRoleDto> Create(ExternalRoleDto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<ExternalRoleDto> Get(int id);
        Task<PagedResultDto<ExternalRoleDto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<ExternalRoleDto> Update(ExternalRoleDto input);
    }
}