﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.EquipItems;

namespace ASEAPROJECT.Services.EquipItemService
{
    public interface IEquipItemService
    {
        Task<EquipItemDto> Create(EquipItemDto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<EquipItemDto> Get(int id);
        Task<PagedResultDto<EquipItemDto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<EquipItemDto> Update(EquipItemDto input);
    }
}