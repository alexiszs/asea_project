﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.Dispenser_Item_in_Logs;

namespace ASEAPROJECT.Services.Dispenser_Item_in_LogService
{
    public interface IDispenser_Item_in_LogService
    {
        Task<Dispenser_Item_in_Log_Dto> Create(Dispenser_Item_in_Log_Dto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<Dispenser_Item_in_Log_Dto> Get(EntityDto<int> input);
        Task<PagedResultDto<Dispenser_Item_in_Log_Dto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<Dispenser_Item_in_Log_Dto> Update(Dispenser_Item_in_Log_Dto input);
        Task<List<Dispenser_Item_in_Log_Dto>> GetAllByLog(int id);
    }
}