﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.GroceryItem;

namespace ASEAPROJECT.Services.GroceryItemService
{
    public interface IGroceryItemService
    {
        Task<GroceryItemsDto> Create(GroceryItemsDto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<GroceryItemsDto> Get(int id);
        Task<PagedResultDto<GroceryItemsDto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<GroceryItemsDto> Update(GroceryItemsDto input);
    }
}