﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.Womens_Bathroom_in_Logs;

namespace ASEAPROJECT.Services.Womens_Bathroom_in_LogService
{
    public interface IWomens_Bathroom_in_LogService
    {
        Task<Womens_Bathroom_in_Log_Dto> Create(Womens_Bathroom_in_Log_Dto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<Womens_Bathroom_in_Log_Dto> Get(EntityDto<int> input);
        Task<PagedResultDto<Womens_Bathroom_in_Log_Dto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<Womens_Bathroom_in_Log_Dto> Update(Womens_Bathroom_in_Log_Dto input);
        Task<List<Womens_Bathroom_in_Log_Dto>> GetAllByLog(int id);
    }
}