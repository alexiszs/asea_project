﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ASEAPROJECT.Dtos.Dispatch_Item_in_Logs;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEAPROJECT.Services.Dispatch_Item_in_LogService
{
    public class Dispatch_Item_in_LogService : AsyncCrudAppService<Dispatch_Item_in_Log, Dispatch_Item_in_Log_Dto>, IDispatch_Item_in_LogService
    {
        public Dispatch_Item_in_LogService(IRepository<Dispatch_Item_in_Log, int> repository) : base(repository)
        {
        }

        protected override string GetPermissionName { get => base.GetPermissionName; set => base.GetPermissionName = value; }
        protected override string GetAllPermissionName { get => base.GetAllPermissionName; set => base.GetAllPermissionName = value; }
        protected override string CreatePermissionName { get => base.CreatePermissionName; set => base.CreatePermissionName = value; }
        protected override string UpdatePermissionName { get => base.UpdatePermissionName; set => base.UpdatePermissionName = value; }
        protected override string DeletePermissionName { get => base.DeletePermissionName; set => base.DeletePermissionName = value; }

        public override Task<Dispatch_Item_in_Log_Dto> Create(Dispatch_Item_in_Log_Dto input)
        {
            return base.Create(input);
        }

        public override Task Delete(EntityDto<int> input)
        {
            return base.Delete(input);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override Task<Dispatch_Item_in_Log_Dto> Get(EntityDto<int> input)
        {
            return base.Get(input);
        }

        public override Task<PagedResultDto<Dispatch_Item_in_Log_Dto>> GetAll(PagedAndSortedResultRequestDto input)
        {
            return base.GetAll(input);
        }

        public async Task<List<Dispatch_Item_in_Log_Dto>> GetAllByLog(int id)
        {
            return (await GetAll(new PagedAndSortedResultRequestDto())).Items.ToList().Where(i => i.log_id == id).ToList();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override Task<Dispatch_Item_in_Log_Dto> Update(Dispatch_Item_in_Log_Dto input)
        {
            return base.Update(input);
        }

        protected override IQueryable<Dispatch_Item_in_Log> ApplyPaging(IQueryable<Dispatch_Item_in_Log> query, PagedAndSortedResultRequestDto input)
        {
            return base.ApplyPaging(query, input);
        }

        protected override IQueryable<Dispatch_Item_in_Log> ApplySorting(IQueryable<Dispatch_Item_in_Log> query, PagedAndSortedResultRequestDto input)
        {
            return base.ApplySorting(query, input);
        }

        protected override void CheckCreatePermission()
        {
            base.CheckCreatePermission();
        }

        protected override void CheckDeletePermission()
        {
            base.CheckDeletePermission();
        }

        protected override void CheckGetAllPermission()
        {
            base.CheckGetAllPermission();
        }

        protected override void CheckGetPermission()
        {
            base.CheckGetPermission();
        }

        protected override void CheckPermission(string permissionName)
        {
            base.CheckPermission(permissionName);
        }

        protected override void CheckUpdatePermission()
        {
            base.CheckUpdatePermission();
        }

        protected override IQueryable<Dispatch_Item_in_Log> CreateFilteredQuery(PagedAndSortedResultRequestDto input)
        {
            return base.CreateFilteredQuery(input);
        }

        protected override Task<Dispatch_Item_in_Log> GetEntityByIdAsync(int id)
        {
            return base.GetEntityByIdAsync(id);
        }

        protected override bool IsEnabled(string featureName)
        {
            return base.IsEnabled(featureName);
        }

        protected override Task<bool> IsEnabledAsync(string featureName)
        {
            return base.IsEnabledAsync(featureName);
        }

        protected override bool IsGranted(string permissionName)
        {
            return base.IsGranted(permissionName);
        }

        protected override Task<bool> IsGrantedAsync(string permissionName)
        {
            return base.IsGrantedAsync(permissionName);
        }

        protected override string L(string name)
        {
            return base.L(name);
        }

        protected override string L(string name, CultureInfo culture)
        {
            return base.L(name, culture);
        }

        protected override Dispatch_Item_in_Log MapToEntity(Dispatch_Item_in_Log_Dto createInput)
        {
            return base.MapToEntity(createInput);
        }

        protected override void MapToEntity(Dispatch_Item_in_Log_Dto updateInput, Dispatch_Item_in_Log entity)
        {
            base.MapToEntity(updateInput, entity);
        }

        protected override Dispatch_Item_in_Log_Dto MapToEntityDto(Dispatch_Item_in_Log entity)
        {
            return base.MapToEntityDto(entity);
        }
    }
}
