﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.Dispatch_Item_in_Logs;

namespace ASEAPROJECT.Services.Dispatch_Item_in_LogService
{
    public interface IDispatch_Item_in_LogService
    {
        Task<Dispatch_Item_in_Log_Dto> Create(Dispatch_Item_in_Log_Dto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<Dispatch_Item_in_Log_Dto> Get(EntityDto<int> input);
        Task<PagedResultDto<Dispatch_Item_in_Log_Dto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<Dispatch_Item_in_Log_Dto> Update(Dispatch_Item_in_Log_Dto input);
        Task<List<Dispatch_Item_in_Log_Dto>> GetAllByLog(int id);
    }
}