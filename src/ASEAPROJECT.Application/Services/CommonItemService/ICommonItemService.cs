﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.CommonItemDto;

namespace ASEAPROJECT.Services.CommonItemService
{
    public interface ICommonItemService
    {
        Task<CommonItemsDto> Create(CommonItemsDto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<CommonItemsDto> Get(int id);
        Task<PagedResultDto<CommonItemsDto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<CommonItemsDto> Update(CommonItemsDto input);
    }
}