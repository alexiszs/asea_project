﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.Extinguisher_Item_in_Logs;

namespace ASEAPROJECT.Services.Extinguisher_Item_in_LogService
{
    public interface IExtinguisher_Item_in_LogService
    {
        Task<Extinguisher_Item_in_Log_Dto> Create(Extinguisher_Item_in_Log_Dto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<Extinguisher_Item_in_Log_Dto> Get(EntityDto<int> input);
        Task<PagedResultDto<Extinguisher_Item_in_Log_Dto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<Extinguisher_Item_in_Log_Dto> Update(Extinguisher_Item_in_Log_Dto input);
        Task<List<Extinguisher_Item_in_Log_Dto>> GetAllByLog(int id);
    }
}