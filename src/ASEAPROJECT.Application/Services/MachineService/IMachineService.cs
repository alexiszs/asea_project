﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.Machines;

namespace ASEAPROJECT.Services.MachineService
{
    public interface IMachineService
    {
        Task<MachineDto> Create(MachineDto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<MachineDto> Get(int ind);
        Task<PagedResultDto<MachineDto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<MachineDto> Update(MachineDto input);
    }
}