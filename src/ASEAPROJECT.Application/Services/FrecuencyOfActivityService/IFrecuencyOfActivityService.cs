﻿using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.FrecuencyOfActivities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ASEAPROJECT.Services.FrecuencyOfActivityService
{
    public interface IFrecuencyOfActivityService
    {
        Task<FrecuencyOfActivityDto> Create(FrecuencyOfActivityDto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<FrecuencyOfActivityDto> Get(int id);
        Task<PagedResultDto<FrecuencyOfActivityDto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<FrecuencyOfActivityDto> Update(FrecuencyOfActivityDto input);
    }
}
