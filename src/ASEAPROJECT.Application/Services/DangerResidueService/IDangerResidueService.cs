﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.DangerResidue;

namespace ASEAPROJECT.Services.DangerResidueService
{
    public interface IDangerResidueService
    {
        Task<DangerResidueDto> Create(DangerResidueDto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<DangerResidueDto> Get(int id);
        Task<PagedResultDto<DangerResidueDto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<DangerResidueDto> Update(DangerResidueDto input);
    }
}