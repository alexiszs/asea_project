﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Abp.Domain.Repositories;
using ASEAPROJECT.Entities;
using System.Linq;

namespace ASEAPROJECT.Services.SelectListService
{
    public class SelectListService : ASEAPROJECTAppServiceBase, ISelectListService
    {
        private readonly IRepository<ExternalFirm> _firmsRepository;
        private readonly IRepository<Event> _eventsRespository;
        private readonly IRepository<FrecuencyOfActivity> _frecuenciesRepository;


        public SelectListService(IRepository<ExternalFirm> firmsRepository, IRepository<Event> eventsRespository, IRepository<FrecuencyOfActivity> frecuenciesRepository)
        {
            _firmsRepository = firmsRepository;
            _eventsRespository = eventsRespository;
            _frecuenciesRepository = frecuenciesRepository;
        }

        public SelectList GetEventsList()
        {
            var list = _eventsRespository.GetAll().Select(u => new { id = u.Id, name = u.name });
            return new SelectList(list, "id", "name");
        }

        public SelectList GetFirmsList()
        {
            var list = _firmsRepository.GetAll().Select(f => new { id = f.Id, name = f.company_name });
            return new SelectList(list, "id", "name");
        }

        public SelectList GetFrecuenciesList()
        {
            var list = _frecuenciesRepository.GetAll().Select(fr => new { id = fr.Id, name = fr.name });
            return new SelectList(list, "id", "name");
        }
    }
}
