﻿using Abp.Application.Services;
using ASEAPROJECT.Dtos.Logs;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace ASEAPROJECT.Services.SelectListService
{
    public interface ISelectListService : IApplicationService
    {
        SelectList GetFirmsList();
        SelectList GetEventsList();
        SelectList GetFrecuenciesList();

    }
}
