﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.TemporyStorages;

namespace ASEAPROJECT.Services.TemporqaryStorageService
{
    public interface ITemporaryStorageService
    {
        Task<TemporyStorageDto> Create(TemporyStorageDto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<TemporyStorageDto> Get(int id);
        Task<PagedResultDto<TemporyStorageDto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<TemporyStorageDto> Update(TemporyStorageDto input);
    }
}