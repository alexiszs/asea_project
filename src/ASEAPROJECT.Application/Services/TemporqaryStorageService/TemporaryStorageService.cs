﻿using Abp.Application.Services;
using ASEAPROJECT.Dtos.TemporyStorages;
using ASEAPROJECT.Entities;
using System.Collections.Generic;
using Abp.Domain.Repositories;
using Abp.Application.Services.Dto;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace ASEAPROJECT.Services.TemporqaryStorageService
{
    public class TemporaryStorageService : AsyncCrudAppService<TemporyStorage, TemporyStorageDto>, ITemporaryStorageService
    {
        public TemporaryStorageService(IRepository<TemporyStorage, int> repository) : base(repository)
        {
        }

        protected override string GetPermissionName { get => base.GetPermissionName; set => base.GetPermissionName = value; }
        protected override string GetAllPermissionName { get => base.GetAllPermissionName; set => base.GetAllPermissionName = value; }
        protected override string CreatePermissionName { get => base.CreatePermissionName; set => base.CreatePermissionName = value; }
        protected override string UpdatePermissionName { get => base.UpdatePermissionName; set => base.UpdatePermissionName = value; }
        protected override string DeletePermissionName { get => base.DeletePermissionName; set => base.DeletePermissionName = value; }

        public override Task<TemporyStorageDto> Create(TemporyStorageDto input)
        {
            return base.Create(input);
        }

        public override Task Delete(EntityDto<int> input)
        {
            return base.Delete(input);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public async Task<TemporyStorageDto> Get(int id)
        {
            var temporaryStorage = await GetAll(new PagedAndSortedResultRequestDto());
            var list = new List<TemporyStorageDto>();
            list = (List<TemporyStorageDto>)temporaryStorage.Items;
            return list.FirstOrDefault(t => t.Id == id);
        }

        public override Task<PagedResultDto<TemporyStorageDto>> GetAll(PagedAndSortedResultRequestDto input)
        {
            return base.GetAll(input);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override Task<TemporyStorageDto> Update(TemporyStorageDto input)
        {
            return base.Update(input);
        }

        protected override IQueryable<TemporyStorage> ApplyPaging(IQueryable<TemporyStorage> query, PagedAndSortedResultRequestDto input)
        {
            return base.ApplyPaging(query, input);
        }

        protected override IQueryable<TemporyStorage> ApplySorting(IQueryable<TemporyStorage> query, PagedAndSortedResultRequestDto input)
        {
            return base.ApplySorting(query, input);
        }

        protected override void CheckCreatePermission()
        {
            base.CheckCreatePermission();
        }

        protected override void CheckDeletePermission()
        {
            base.CheckDeletePermission();
        }

        protected override void CheckGetAllPermission()
        {
            base.CheckGetAllPermission();
        }

        protected override void CheckGetPermission()
        {
            base.CheckGetPermission();
        }

        protected override void CheckPermission(string permissionName)
        {
            base.CheckPermission(permissionName);
        }

        protected override void CheckUpdatePermission()
        {
            base.CheckUpdatePermission();
        }

        protected override IQueryable<TemporyStorage> CreateFilteredQuery(PagedAndSortedResultRequestDto input)
        {
            return base.CreateFilteredQuery(input);
        }

        protected override Task<TemporyStorage> GetEntityByIdAsync(int id)
        {
            return base.GetEntityByIdAsync(id);
        }

        protected override bool IsEnabled(string featureName)
        {
            return base.IsEnabled(featureName);
        }

        protected override Task<bool> IsEnabledAsync(string featureName)
        {
            return base.IsEnabledAsync(featureName);
        }

        protected override bool IsGranted(string permissionName)
        {
            return base.IsGranted(permissionName);
        }

        protected override Task<bool> IsGrantedAsync(string permissionName)
        {
            return base.IsGrantedAsync(permissionName);
        }

        protected override string L(string name)
        {
            return base.L(name);
        }

        protected override string L(string name, CultureInfo culture)
        {
            return base.L(name, culture);
        }

        protected override TemporyStorage MapToEntity(TemporyStorageDto createInput)
        {
            return base.MapToEntity(createInput);
        }

        protected override void MapToEntity(TemporyStorageDto updateInput, TemporyStorage entity)
        {
            base.MapToEntity(updateInput, entity);
        }

        protected override TemporyStorageDto MapToEntityDto(TemporyStorage entity)
        {
            return base.MapToEntityDto(entity);
        }
    }
}
