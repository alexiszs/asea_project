﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.DailyVerificationLogs;

namespace ASEAPROJECT.Services.DailyVerificationLogService
{
    public interface IDailyVerificationService
    {
        Task<DailyVerifcationLogDto> Create(DailyVerifcationLogDto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<DailyVerifcationLogDto> Get(int id);
        Task<PagedResultDto<DailyVerifcationLogDto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<DailyVerifcationLogDto> Update(DailyVerifcationLogDto input);
    }
}