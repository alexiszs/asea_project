﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ASEAPROJECT.Dtos.ExtinguisherItems;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEAPROJECT.Services.ExtinguisherItemService
{
    public class ExtinguisherItemService : AsyncCrudAppService<ExtinguisherItem, ExtinguisherItemDto>, IExtinguisherItemService
    {
        public ExtinguisherItemService(IRepository<ExtinguisherItem, int> repository) : base(repository)
        {
        }

        protected override string GetPermissionName { get => base.GetPermissionName; set => base.GetPermissionName = value; }
        protected override string GetAllPermissionName { get => base.GetAllPermissionName; set => base.GetAllPermissionName = value; }
        protected override string CreatePermissionName { get => base.CreatePermissionName; set => base.CreatePermissionName = value; }
        protected override string UpdatePermissionName { get => base.UpdatePermissionName; set => base.UpdatePermissionName = value; }
        protected override string DeletePermissionName { get => base.DeletePermissionName; set => base.DeletePermissionName = value; }

        public override Task<ExtinguisherItemDto> Create(ExtinguisherItemDto input)
        {
            return base.Create(input);
        }

        public override Task Delete(EntityDto<int> input)
        {
            return base.Delete(input);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public async Task<ExtinguisherItemDto> Get(int id)
        {
            var itemslist = await base.GetAll(new PagedAndSortedResultRequestDto());
            var list = new List<ExtinguisherItemDto>();
            list = (List<ExtinguisherItemDto>)itemslist.Items;
            return list.FirstOrDefault(i => i.Id == id);
        }

        public override Task<PagedResultDto<ExtinguisherItemDto>> GetAll(PagedAndSortedResultRequestDto input)
        {
            return base.GetAll(input);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override Task<ExtinguisherItemDto> Update(ExtinguisherItemDto input)
        {
            return base.Update(input);
        }

        protected override IQueryable<ExtinguisherItem> ApplyPaging(IQueryable<ExtinguisherItem> query, PagedAndSortedResultRequestDto input)
        {
            return base.ApplyPaging(query, input);
        }

        protected override IQueryable<ExtinguisherItem> ApplySorting(IQueryable<ExtinguisherItem> query, PagedAndSortedResultRequestDto input)
        {
            return base.ApplySorting(query, input);
        }

        protected override void CheckCreatePermission()
        {
            base.CheckCreatePermission();
        }

        protected override void CheckDeletePermission()
        {
            base.CheckDeletePermission();
        }

        protected override void CheckGetAllPermission()
        {
            base.CheckGetAllPermission();
        }

        protected override void CheckGetPermission()
        {
            base.CheckGetPermission();
        }

        protected override void CheckPermission(string permissionName)
        {
            base.CheckPermission(permissionName);
        }

        protected override void CheckUpdatePermission()
        {
            base.CheckUpdatePermission();
        }

        protected override IQueryable<ExtinguisherItem> CreateFilteredQuery(PagedAndSortedResultRequestDto input)
        {
            return base.CreateFilteredQuery(input);
        }

        protected override Task<ExtinguisherItem> GetEntityByIdAsync(int id)
        {
            return base.GetEntityByIdAsync(id);
        }

        protected override bool IsEnabled(string featureName)
        {
            return base.IsEnabled(featureName);
        }

        protected override Task<bool> IsEnabledAsync(string featureName)
        {
            return base.IsEnabledAsync(featureName);
        }

        protected override bool IsGranted(string permissionName)
        {
            return base.IsGranted(permissionName);
        }

        protected override Task<bool> IsGrantedAsync(string permissionName)
        {
            return base.IsGrantedAsync(permissionName);
        }

        protected override string L(string name)
        {
            return base.L(name);
        }

        protected override string L(string name, CultureInfo culture)
        {
            return base.L(name, culture);
        }

        protected override ExtinguisherItem MapToEntity(ExtinguisherItemDto createInput)
        {
            return base.MapToEntity(createInput);
        }

        protected override void MapToEntity(ExtinguisherItemDto updateInput, ExtinguisherItem entity)
        {
            base.MapToEntity(updateInput, entity);
        }

        protected override ExtinguisherItemDto MapToEntityDto(ExtinguisherItem entity)
        {
            return base.MapToEntityDto(entity);
        }
    }
}
