﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.ExtinguisherItems;

namespace ASEAPROJECT.Services.ExtinguisherItemService
{
    public interface IExtinguisherItemService
    {
        Task<ExtinguisherItemDto> Create(ExtinguisherItemDto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<ExtinguisherItemDto> Get(int id);
        Task<PagedResultDto<ExtinguisherItemDto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<ExtinguisherItemDto> Update(ExtinguisherItemDto input);
    }
}