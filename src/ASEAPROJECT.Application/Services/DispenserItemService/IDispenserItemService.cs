﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.DispatchItems;
using ASEAPROJECT.Dtos.DispenserItems;

namespace ASEAPROJECT.Services.DispenserItemService
{
    public interface IDispenserItemService
    {
        Task<DispenserItemDto> Create(DispenserItemDto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<DispatchItemDto> Get(int id);
        Task<PagedResultDto<DispenserItemDto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<DispenserItemDto> Update(DispenserItemDto input);
    }
}