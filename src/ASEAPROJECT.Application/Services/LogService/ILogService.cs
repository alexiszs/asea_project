﻿using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.Logs;
using System.Threading.Tasks;

namespace ASEAPROJECT.Services.LogService
{
    public interface ILogService
    {
        Task<LogDto> Create(LogDto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<LogDto> Get(int id);
        Task<PagedResultDto<LogDto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<LogDto> Update(LogDto input);
    }
}
