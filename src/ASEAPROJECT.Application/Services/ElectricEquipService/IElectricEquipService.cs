﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.ElectricEquips;

namespace ASEAPROJECT.Services.ElectricEquipService
{
    public interface IElectricEquipService
    {
        Task<ElectricEquipDto> Create(ElectricEquipDto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<ElectricEquipDto> Get(int id);
        Task<PagedResultDto<ElectricEquipDto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<ElectricEquipDto> Update(ElectricEquipDto input);
    }
}