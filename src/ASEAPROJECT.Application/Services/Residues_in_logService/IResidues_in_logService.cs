﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.Residues_in_Logs;

namespace ASEAPROJECT.Services.Residues_in_logService
{
    public interface IResidues_in_logService
    {
        Task<Residues_in_log_Dto> Create(Residues_in_log_Dto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<Residues_in_log_Dto> Get(EntityDto<int> input);
        Task<PagedResultDto<Residues_in_log_Dto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<Residues_in_log_Dto> Update(Residues_in_log_Dto input);
        Task<List<Residues_in_log_Dto>> GetAllByLog(int id);
    }
}