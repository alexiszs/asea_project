﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ASEAPROJECT.Dtos.Common_item_inLogs;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEAPROJECT.Services.Common_item_in_logService
{
    public class Common_item_in_logService : AsyncCrudAppService<Common_Item_in_log, Common_item_in_log_Dto>, ICommon_item_in_logService
    {
        public Common_item_in_logService(IRepository<Common_Item_in_log, int> repository) : base(repository)
        {
        }

        protected override string GetPermissionName { get => base.GetPermissionName; set => base.GetPermissionName = value; }
        protected override string GetAllPermissionName { get => base.GetAllPermissionName; set => base.GetAllPermissionName = value; }
        protected override string CreatePermissionName { get => base.CreatePermissionName; set => base.CreatePermissionName = value; }
        protected override string UpdatePermissionName { get => base.UpdatePermissionName; set => base.UpdatePermissionName = value; }
        protected override string DeletePermissionName { get => base.DeletePermissionName; set => base.DeletePermissionName = value; }

        public override Task<Common_item_in_log_Dto> Create(Common_item_in_log_Dto input)
        {
            return base.Create(input);
        }

        public override Task Delete(EntityDto<int> input)
        {
            return base.Delete(input);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override Task<Common_item_in_log_Dto> Get(EntityDto<int> input)
        {
            return base.Get(input);
        }

        public async Task<List<Common_item_in_log_Dto>>GetAllByLog(int id)
        {
            var listOfitems = await GetAll(new PagedAndSortedResultRequestDto());
            var list = new List<Common_item_in_log_Dto>();
            list = listOfitems.Items.ToList().Where(i => i.log_id == id).ToList();
            return list;
        }

        public override Task<PagedResultDto<Common_item_in_log_Dto>> GetAll(PagedAndSortedResultRequestDto input)
        {
            return base.GetAll(input);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override Task<Common_item_in_log_Dto> Update(Common_item_in_log_Dto input)
        {
            return base.Update(input);
        }

        protected override IQueryable<Common_Item_in_log> ApplyPaging(IQueryable<Common_Item_in_log> query, PagedAndSortedResultRequestDto input)
        {
            return base.ApplyPaging(query, input);
        }

        protected override IQueryable<Common_Item_in_log> ApplySorting(IQueryable<Common_Item_in_log> query, PagedAndSortedResultRequestDto input)
        {
            return base.ApplySorting(query, input);
        }

        protected override void CheckCreatePermission()
        {
            base.CheckCreatePermission();
        }

        protected override void CheckDeletePermission()
        {
            base.CheckDeletePermission();
        }

        protected override void CheckGetAllPermission()
        {
            base.CheckGetAllPermission();
        }

        protected override void CheckGetPermission()
        {
            base.CheckGetPermission();
        }

        protected override void CheckPermission(string permissionName)
        {
            base.CheckPermission(permissionName);
        }

        protected override void CheckUpdatePermission()
        {
            base.CheckUpdatePermission();
        }

        protected override IQueryable<Common_Item_in_log> CreateFilteredQuery(PagedAndSortedResultRequestDto input)
        {
            return base.CreateFilteredQuery(input);
        }

        protected override Task<Common_Item_in_log> GetEntityByIdAsync(int id)
        {
            return base.GetEntityByIdAsync(id);
        }

        protected override bool IsEnabled(string featureName)
        {
            return base.IsEnabled(featureName);
        }

        protected override Task<bool> IsEnabledAsync(string featureName)
        {
            return base.IsEnabledAsync(featureName);
        }

        protected override bool IsGranted(string permissionName)
        {
            return base.IsGranted(permissionName);
        }

        protected override Task<bool> IsGrantedAsync(string permissionName)
        {
            return base.IsGrantedAsync(permissionName);
        }

        protected override string L(string name)
        {
            return base.L(name);
        }

        protected override string L(string name, CultureInfo culture)
        {
            return base.L(name, culture);
        }

        protected override Common_Item_in_log MapToEntity(Common_item_in_log_Dto createInput)
        {
            return base.MapToEntity(createInput);
        }

        protected override void MapToEntity(Common_item_in_log_Dto updateInput, Common_Item_in_log entity)
        {
            base.MapToEntity(updateInput, entity);
        }

        protected override Common_item_in_log_Dto MapToEntityDto(Common_Item_in_log entity)
        {
            return base.MapToEntityDto(entity);
        }
    }
}
