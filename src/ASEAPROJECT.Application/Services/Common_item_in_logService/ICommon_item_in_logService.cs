﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.Common_item_inLogs;

namespace ASEAPROJECT.Services.Common_item_in_logService
{
    public interface ICommon_item_in_logService
    {
        Task<Common_item_in_log_Dto> Create(Common_item_in_log_Dto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<Common_item_in_log_Dto> Get(EntityDto<int> input);
        Task<PagedResultDto<Common_item_in_log_Dto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<Common_item_in_log_Dto> Update(Common_item_in_log_Dto input);
        Task<List<Common_item_in_log_Dto>> GetAllByLog(int id);
    }
}