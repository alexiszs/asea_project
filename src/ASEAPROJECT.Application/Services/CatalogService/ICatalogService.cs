﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ASEAPROJECT.Dtos.Catalogs;

namespace ASEAPROJECT.Services.CatalogService
{
    interface ICatalogService
    {
        Task<CatalogDto> Create(CatalogDto input);
        Task Delete(EntityDto<int> input);
        bool Equals(object obj);
        Task<CatalogDto> Get(EntityDto<int> input);
        Task<PagedResultDto<CatalogDto>> GetAll(PagedAndSortedResultRequestDto input);
        int GetHashCode();
        string ToString();
        Task<CatalogDto> Update(CatalogDto input);
    }
}