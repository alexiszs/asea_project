﻿using System.Threading.Tasks;
using Abp.Application.Services;
using ASEAPROJECT.Authorization.Accounts.Dto;

namespace ASEAPROJECT.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
