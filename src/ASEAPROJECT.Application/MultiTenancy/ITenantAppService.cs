﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ASEAPROJECT.MultiTenancy.Dto;

namespace ASEAPROJECT.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}
