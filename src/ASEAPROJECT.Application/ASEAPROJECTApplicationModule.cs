﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ASEAPROJECT.Authorization;

namespace ASEAPROJECT
{
    [DependsOn(
        typeof(ASEAPROJECTCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class ASEAPROJECTApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<ASEAPROJECTAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(ASEAPROJECTApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddProfiles(thisAssembly)
            );
        }
    }
}
