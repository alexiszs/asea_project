﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using ASEAPROJECT.Configuration.Dto;

namespace ASEAPROJECT.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : ASEAPROJECTAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
