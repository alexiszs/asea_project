﻿using System.Threading.Tasks;
using ASEAPROJECT.Configuration.Dto;

namespace ASEAPROJECT.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
