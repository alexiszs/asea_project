﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ASEAPROJECT.Dtos.Logs;
using ASEAPROJECT.Dtos.RolesFirm;
using ASEAPROJECT.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ASEAPROJECT.Dtos.ExternalFirms
{
    [AutoMap(typeof(ExternalFirm))]
    public class ExternalFirmDto : EntityDto
    {
        [Required]
        public string tradename { get; set; }

        [Required]
        public string company_name { get; set; }

        [Required]
        public string addres { get; set; }

        [Required]
        public string email { get; set; }

        public virtual ICollection<LogDto> logs { get; set; }

        public ICollection<RolesFirms> roles { get; set; }
    }
}
