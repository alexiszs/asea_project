﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASEAPROJECT.Dtos.RolesFirm
{
    [AutoMap(typeof(RolesFirms))]
    public class RolesFirmsDto : EntityDto
    {

        public int firm_id { get; set; }

        public int role_id { get; set; }
    }
}
