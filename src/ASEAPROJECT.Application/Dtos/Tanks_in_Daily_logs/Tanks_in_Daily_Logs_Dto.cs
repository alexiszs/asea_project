﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ASEAPROJECT.Dtos.DailyVerificationLogs;
using ASEAPROJECT.Dtos.Tanks;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASEAPROJECT.Dtos.Tanks_in_Daily_logs
{
    [AutoMap(typeof(Tanks_in_Daily_Logs))]
    public class Tanks_in_Daily_Logs_Dto: EntityDto
    {
        public int log_id { get; set; }

        public int tank_id { get; set; }

        public DailyVerifcationLogDto log { get; set; }

        public TankDto tank { get; set; }
    }
}
