﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ASEAPROJECT.Dtos.DailyVerificationLogs;
using ASEAPROJECT.Dtos.MensBathrooms;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASEAPROJECT.Dtos.Mens_Bathroom_In_Logs
{
    [AutoMap(typeof(Mens_Bathroom_in_Log))]
     public class Mens_bathroom_In_Log_Dto : EntityDto
    {
        public int log_id { get; set; }

        public int bathroom_id { get; set; }

        public DailyVerifcationLogDto log { get; set; }

        public MensBathroomDto bathroom { get; set; }
    }
}
