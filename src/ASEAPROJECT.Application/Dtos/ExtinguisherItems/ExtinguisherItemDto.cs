﻿using Abp.AutoMapper;
using ASEAPROJECT.Dtos.Catalogs;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASEAPROJECT.Dtos.ExtinguisherItems
{
    [AutoMap(typeof(ExtinguisherItem))]
    public class ExtinguisherItemDto : CatalogDto
    {
    }
}
