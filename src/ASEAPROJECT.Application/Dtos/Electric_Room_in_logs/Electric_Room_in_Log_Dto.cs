﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ASEAPROJECT.Dtos.DailyVerificationLogs;
using ASEAPROJECT.Dtos.ElectricEquips;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASEAPROJECT.Dtos.Electric_Room_in_logs
{
    [AutoMap(typeof(Electric_Room_in_Log))]
    public class Electric_Room_in_Log_Dto : EntityDto
    {
        public int log_id { get; set; }

        public int equip_id { get; set; }

        public DailyVerifcationLogDto log { get; set; }

        public ElectricEquipDto equip { get; set; }
    }
}
