﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ASEAPROJECT.Dtos.Catalogs
{
    [AutoMap(typeof(Catalog))]
    public abstract class CatalogDto : EntityDto
    {
        [Required]
        public string name { get; set; }
    }
}
