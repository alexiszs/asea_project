﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ASEAPROJECT.Dtos.DailyVerificationLogs;
using ASEAPROJECT.Dtos.DangerResidue;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASEAPROJECT.Dtos.Residues_in_Logs
{
    [AutoMap(typeof(Residues_in_Log))]
    public class Residues_in_log_Dto: EntityDto
    {
        public int log_id { get; set; }

        public int residue_id { get; set; }

        public DailyVerifcationLogDto log { get; set; }

        public DangerResidueDto residue { get; set; }
    }
}
