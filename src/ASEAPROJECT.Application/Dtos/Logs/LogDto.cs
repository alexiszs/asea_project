﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ASEAPROJECT.Dtos.Events;
using ASEAPROJECT.Dtos.ExternalFirms;
using ASEAPROJECT.Dtos.FrecuencyOfActivities;
using ASEAPROJECT.Entities;
using System;
using System.ComponentModel.DataAnnotations;

namespace ASEAPROJECT.Dtos.Logs
{
    [AutoMap(typeof(Log))]
    public class LogDto : AuditedEntityDto
    {
        public DateTime date { get; set; }
      
        public DateTime begin { get; set; }

        public DateTime end { get; set; }

        public string certication_number { get; set; }

        public string comments { get; set; }

        public int frecuency_id { get; set; }

        public int event_type_id { get; set; }

        public int firm_id { get; set; }

        public FrecuencyOfActivityDto frecuency { get; set; }

        public EventDto event_type { get; set; }

        public ExternalFirmDto firm { get; set; }

        public virtual bool IsDeleted { get; set; }
    }
}
