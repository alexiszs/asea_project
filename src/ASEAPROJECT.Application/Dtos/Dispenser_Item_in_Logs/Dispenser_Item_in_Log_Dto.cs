﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ASEAPROJECT.Dtos.DailyVerificationLogs;
using ASEAPROJECT.Dtos.DispenserItems;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASEAPROJECT.Dtos.Dispenser_Item_in_Logs
{
    [AutoMap(typeof(Dispenser_Item_in_Log))]
    public class Dispenser_Item_in_Log_Dto : EntityDto
    {
        public int log_id { get; set; }

        public int item_id { get; set; }

        public DailyVerifcationLogDto log { get; set; }

        public DispenserItemDto item { get; set; }
    }
}
