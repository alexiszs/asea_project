﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ASEAPROJECT.Dtos.DailyVerificationLogs;
using ASEAPROJECT.Dtos.GroceryItem;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASEAPROJECT.Dtos.Grocery_Item_in_Logs
{
    [AutoMap(typeof(Grocery_Item_in_log))]
    public class Grocery_Item_in_Log_Dto : EntityDto
    {
        public int log_id { get; set; }

        public int item_id { get; set; }

        public DailyVerifcationLogDto log { get; set; }

        public GroceryItemsDto item { get; set; }
    }
}
