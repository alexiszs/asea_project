﻿using Abp.AutoMapper;
using ASEAPROJECT.Dtos.Catalogs;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASEAPROJECT.Dtos.Events
{
    [AutoMap(typeof(Event))]
    public class EventDto : CatalogDto
    {
    }
}
