﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ASEAPROJECT.Dtos.CommonItemDto;
using ASEAPROJECT.Dtos.DailyVerificationLogs;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASEAPROJECT.Dtos.Common_item_inLogs
{
    [AutoMap(typeof(Common_Item_in_log))]
    public class Common_item_in_log_Dto: EntityDto
    {
        public int log_id { get; set; }

        public int item_id { get; set; }

        public DailyVerifcationLogDto log { get; set; }

        public CommonItemsDto item { get; set; }
    }
}
