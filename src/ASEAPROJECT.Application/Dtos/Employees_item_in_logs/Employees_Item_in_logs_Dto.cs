﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ASEAPROJECT.Dtos.DailyVerificationLogs;
using ASEAPROJECT.Dtos.EmployeItems;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASEAPROJECT.Dtos.Employees_item_in_logs
{
    [AutoMap(typeof(Employees_Item_in_Log))]
    public class Employees_Item_in_logs_Dto : EntityDto
    {
        public int log_id { get; set; }

        public int item_id { get; set; }

        public DailyVerifcationLogDto log { get; set; }

        public EmployeItemDto item { get; set; }
    }
}
