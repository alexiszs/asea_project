﻿using Abp.AutoMapper;
using ASEAPROJECT.Dtos.Catalogs;
using System;
using System.Collections.Generic;
using System.Reflection.PortableExecutable;
using System.Text;

namespace ASEAPROJECT.Dtos.Machines
{
    [AutoMap(typeof(Machine))]
    public class MachineDto : CatalogDto
    {
    }
}
