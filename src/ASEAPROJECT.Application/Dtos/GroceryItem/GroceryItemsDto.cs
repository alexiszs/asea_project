﻿using Abp.AutoMapper;
using ASEAPROJECT.Dtos.Catalogs;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASEAPROJECT.Dtos.GroceryItem
{
    [AutoMap(typeof(GroceryItems))]
    public class GroceryItemsDto : CatalogDto
    {
    }
}
