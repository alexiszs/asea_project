﻿using Abp.AutoMapper;
using ASEAPROJECT.Dtos.Catalogs;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASEAPROJECT.Dtos.TemporyStorages
{
    [AutoMap(typeof(TemporyStorage))]
    public class TemporyStorageDto : CatalogDto
    {
    }
}
