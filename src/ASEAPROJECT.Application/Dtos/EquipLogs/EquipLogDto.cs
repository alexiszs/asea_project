﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASEAPROJECT.Dtos.EquipLogs
{
    [AutoMap(typeof(EquipLog))]
    public class EquipLogDto : EntityDto
    {
        public int residual_log_id { get; set; }

        public int equip_id { get; set; }
    }
}
