﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ASEAPROJECT.Dtos.DailyVerificationLogs;
using ASEAPROJECT.Dtos.ExtinguisherItems;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASEAPROJECT.Dtos.Extinguisher_Item_in_Logs
{
    [AutoMap(typeof(Extinguisher_Item_in_Log))]
    public class Extinguisher_Item_in_Log_Dto : EntityDto
    {
        public int log_id { get; set; }

        public int item_id { get; set; }

        public DailyVerifcationLogDto log { get; set; }

        public ExtinguisherItemDto item { get; set; }
    }
}
