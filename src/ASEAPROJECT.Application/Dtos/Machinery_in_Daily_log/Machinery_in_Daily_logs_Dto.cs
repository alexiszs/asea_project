﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ASEAPROJECT.Dtos.DailyVerificationLogs;
using ASEAPROJECT.Dtos.Machines;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASEAPROJECT.Dtos.Machinery_in_Daily_log
{
    [AutoMap(typeof(Machinery_in_Daily_logs))]
    public class Machinery_in_Daily_logs_Dto : EntityDto
    {
        public int log_id { get; set; }

        public int machine_id { get; set; }

        public DailyVerifcationLogDto log { get; set; }


        public MachineDto machine { get; set; }
    }
}
