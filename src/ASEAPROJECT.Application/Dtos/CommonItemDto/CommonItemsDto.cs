﻿using Abp.AutoMapper;
using ASEAPROJECT.Dtos.Catalogs;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASEAPROJECT.Dtos.CommonItemDto
{
    [AutoMap(typeof(CommonItems))]
    public class CommonItemsDto : CatalogDto
    {
    }
}
