﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ASEAPROJECT.Dtos.Catalogs;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASEAPROJECT.Dtos.ExternalRoles
{
    [AutoMap(typeof(ExternalRol))]
    public class ExternalRoleDto: EntityDto
    {
        public string name { get; set; }
    }
}
