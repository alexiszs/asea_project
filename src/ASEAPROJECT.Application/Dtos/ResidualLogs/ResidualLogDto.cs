﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;

namespace ASEAPROJECT.Dtos.ResidualLogs
{
    [AutoMap(typeof(ResidualLogDto))]
    public class ResidualLogDto:AuditedEntityDto
    {
        public string residueType { get; set; }

        public Byte personalAuth { get; set; }

        public string civilpermission { get; set; }

        public string certnumber { get; set; }

        public string manifestnumber { get; set; }

        public DateTime emissiondate { get; set; }

        public int solidrests { get; set; }

        public int liquidrests { get; set; }

        public int extraction_firm_id { get; set; }

        public int shipping_firm_id { get; set; }

        public int reception_firm_id { get; set; }

        public int temp_storage_id { get; set; }

        public string comments { get; set; }

        public ExternalFirm extraction_firm { get; set; }

        public ExternalFirm shipping_firm { get; set; }

        public ExternalFirm reception_firm { get; set; }

        public TemporyStorage tempory_storage { get; set; }

        public ICollection<EquipLog> equipment { get; set; }

        public virtual bool IsDeleted { get; set; }
    }
}
