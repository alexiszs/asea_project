﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using ASEAPROJECT.Entities;
using ASEAPROJECT.Dtos.Catalogs;

namespace ASEAPROJECT.Dtos.EquipItems
{
    [AutoMap(typeof(EquipItem))]
    public class EquipItemDto : CatalogDto
    {
    }
}
