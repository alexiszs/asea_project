﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ASEAPROJECT.Dtos.DailyVerificationLogs;
using ASEAPROJECT.Dtos.WomensBathrooms;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASEAPROJECT.Dtos.Womens_Bathroom_in_Logs
{
    [AutoMap(typeof(Womens_Bathroom_in_Log))]
    public class Womens_Bathroom_in_Log_Dto : EntityDto
    {
        public int log_id { get; set; }

        public int bathroom_id { get; set; }

        public DailyVerifcationLogDto log { get; set; }

        public WomensBathroomDto WBathroom { get; set; }
    }
}
