﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ASEAPROJECT.Dtos.Common_item_inLogs;
using ASEAPROJECT.Dtos.Dispatch_Item_in_Logs;
using ASEAPROJECT.Dtos.Dispenser_Item_in_Logs;
using ASEAPROJECT.Dtos.Electric_Room_in_logs;
using ASEAPROJECT.Dtos.Employees_item_in_logs;
using ASEAPROJECT.Dtos.Extinguisher_Item_in_Logs;
using ASEAPROJECT.Dtos.Grocery_Item_in_Logs;
using ASEAPROJECT.Dtos.Machinery_in_Daily_log;
using ASEAPROJECT.Dtos.Mens_Bathroom_In_Logs;
using ASEAPROJECT.Dtos.Residues_in_Logs;
using ASEAPROJECT.Dtos.Tanks_in_Daily_logs;
using ASEAPROJECT.Dtos.Womens_Bathroom_in_Logs;
using ASEAPROJECT.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASEAPROJECT.Dtos.DailyVerificationLogs
{
    [AutoMap(typeof(DailyVerificationLog))]
    public class DailyVerifcationLogDto : AuditedEntityDto
    {
        public DateTime date { get; set; }

        public DateTime hour { get; set; }

        public string folio { get; set; }

        public ICollection<Tanks_in_Daily_Logs_Dto> tanks { get; set; }

        public ICollection<Machinery_in_Daily_logs_Dto> machinary { get; set; }

        public ICollection<Residues_in_log_Dto> residues { get; set; }

        public ICollection<Mens_bathroom_In_Log_Dto> Mbathroom { get; set; }

        public ICollection<Womens_Bathroom_in_Log_Dto> Wbathroom { get; set; }

        public ICollection<Electric_Room_in_Log_Dto> ElectrictRoom { get; set; }

        public ICollection<Grocery_Item_in_Log_Dto> grocery { get; set; }

        public ICollection<Common_item_in_log_Dto> common { get; set; }

        public ICollection<Dispenser_Item_in_Log_Dto> dispenser { get; set; }

        public ICollection<Dispatch_Item_in_Log_Dto> dispatch { get; set; }

        public ICollection<Employees_Item_in_logs_Dto> employees { get; set; }

        public ICollection<Extinguisher_Item_in_Log_Dto> extinguisher { get; set; }

        public string comments { get; set; }
        public bool IsDeleted { get; set; }
    }
}
